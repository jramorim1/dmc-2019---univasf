
# coding: utf-8

# ## Gerando conjuntos de treinamento

# In[74]:


import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split


# In[75]:


df = pd.read_csv('Datasets/train.csv', sep='|')
features_to_select = ['trustLevel', 'totalScanTimeInSeconds', 'grandTotal', 'lineItemVoids',
                          'scansWithoutRegistration', 'quantityModifications', 
                          'scannedLineItemsPerSecond', 'valuePerSecond', 'lineItemVoidsPerPosition']
# Definir a semente da geração
random = 42

X = df[df.columns[:-1].tolist()]
X = X[features_to_select]
y = df[df.columns[-1]]

# Divindo os dados em 33.3%
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3333333333, random_state=random, stratify=y)


# In[73]:


# O primeiro conjunto
X_test_1 = y_test.copy(deep=True)
X_train_1 = X_test.copy(deep=True).join(X_test_1)


# Divindo os 66.6% em dois grupos
y_train = pd.DataFrame(y_train)
X_train, X_test, y_train, y_test = train_test_split(X_train, y_train, test_size=0.5, random_state=random, stratify=y_train)

# O segundo conjunto
X_test_2 = y_train.copy(deep=True)
X_train_2 = X_train.copy(deep=True).join(X_test_2)

# O terceiro conjunto
X_test_3 = y_test.copy(deep=True)
X_train_3 = X_test.copy(deep=True).join(X_test_3)


# ### Verificando se foram gerados os folds corretamente

# In[69]:


print(f"True class percentage in train set: {100*X_train_1.sum()/X_train_1.count()}%")
X_train_1.describe()
X_train_1.head()


# In[70]:


print(f"True class percentage in train set: {100*X_train_2.sum()/X_train_2.count()}%")
X_train_2.describe()
X_train_2.head()


# In[71]:


print(f"True class percentage in train set: {100*X_train_3.sum()/X_train_3.count()}%")
X_train_3.describe()
X_train_3.head()


# ### Salvando dados

# In[72]:


X_train_1.to_csv('Datasets/random_state_' + str(random) + '/fold1/tst1.csv', sep='|')
X_train_2.append(X_train_3).to_csv('Datasets/random_state_' + str(random) + '/fold1/trn1.csv', sep='|')

X_train_2.to_csv('Datasets/random_state_' + str(random) + '/fold2/tst2.csv', sep='|')
X_train_1.append(X_train_3).to_csv('Datasets/random_state_' + str(random) + '/fold2/trn2.csv', sep='|')

X_train_3.to_csv('Datasets/random_state_0/fold3/tst3.csv', sep='|')
X_train_1.append(X_train_2).to_csv('Datasets/random_state_' + str(random) + '/fold3/trn3.csv', sep='|')


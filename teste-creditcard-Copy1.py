#!/usr/bin/env python
# coding: utf-8

# In[19]:


import pandas as pd
import numpy as np
import os

from sklearn.preprocessing import StandardScaler, RobustScaler
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.metrics import f1_score, roc_auc_score
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.neural_network import MLPClassifier

from imblearn.pipeline import make_pipeline as make_pipeline_imb
from imblearn.over_sampling import SMOTE
from imblearn.metrics import classification_report_imbalanced

import xgboost as xgb
from xgboost import XGBClassifier

import matplotlib.pyplot as plt
from collections import Counter

import time
import warnings
# warnings.filterwarnings('ignore')


# In[6]:


def data_processor():
    df = pd.read_csv('Datasets/train.csv', sep='|')
#     df = df.drop('Time', axis=1)
    features_to_select = ['trustLevel', 'totalScanTimeInSeconds', 'grandTotal', 'lineItemVoids',
                          'scansWithoutRegistration', 'quantityModifications', 
                          'scannedLineItemsPerSecond', 'valuePerSecond', 'lineItemVoidsPerPosition']

    print('Use StandardScaler to process the column data')
    scaler = StandardScaler()
#     scaler = RobustScaler()
    df[df.columns[:-1].tolist()] = scaler.fit_transform(df[df.columns[:-1].tolist()])
    # print(df.head(5))
    X = df[df.columns[:-1].tolist()]
    X = X[features_to_select]
    y = df[df.columns[-1]]

    print("Train Test Split ratio is 0.33")
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=0, stratify=y)
    print(f"X_train shape: {X_train.shape}")
    print(f"X_test shape: {X_test.shape}")
    print(f"Class percentage in train set: {100*y_train.sum()/y_train.count()}%")
    
    print(f"y_train shape: {y_train.shape}")
    print(f"y_test shape: {y_test.shape}")
    print(f"Class percentage in test set: {100*y_test.sum()/y_test.count()}%")

    return X_train, X_test, y_train, y_test


# ## Objective and Evaluation Functions

# In[20]:


# baseado em https://github.com/dmlc/xgboost/blob/master/demo/guide-python/custom_objective.py
# user define objective function, given prediction, return gradient and second order gradient
# this is log likelihood loss
def logregobj(preds, dtrain):
    labels = dtrain.get_label()
    preds = 1.0 / (1.0 + np.exp(-preds))
    grad = preds - labels
    hess = preds * (1.0 - preds)
    return grad, hess

def logreg_objective(y_true, y_pred):
    y_pred = 1.0 / (1.0 + np.exp(-y_pred))
    grad = y_pred - y_true
    hess = y_pred * (1.0 - y_pred)
    return grad, hess

# user defined evaluation function, return a pair metric_name, result
# NOTE: when you do customized loss function, the default prediction value is margin
# this may make builtin evaluation metric not function properly
# for example, we are doing logistic loss, the prediction is score before logistic transformation
# the builtin evaluation error assumes input is after logistic transformation
# Take this in mind when you use the customization, and maybe you need write customized evaluation function
def cupEvalScore(y_predicted, y_true):
    labels = y_true.get_label()
    y_predicted = np.round(y_predicted)
    matrix = metrics.confusion_matrix(labels, y_predicted)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
    return 'cupScore', weightSum

def cupEvalScore_negative(y_predicted, y_true):
    labels = y_true.get_label()
    y_predicted = np.round(y_predicted)
    matrix = metrics.confusion_matrix(labels, y_predicted)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,25,5,-5]))
    return 'cupScore', weightSum

# training with customized objective, we can also do step by step training
# simply look at xgboost.py's implementation of train
# bst = xgb.train(param, dtrain, num_round, watchlist, obj=logregobj, feval=evalerror)


# In[ ]:





# In[27]:


def xgb_classifier(X_train, X_test, y_train, y_test, useTrainCV=False, cv_folds=3, early_stopping_rounds=10):
    alg = XGBClassifier(learning_rate=0.01, n_estimators=9001, max_depth=20,
                        min_child_weight=2, gamma=0.2, subsample=0.2, colsample_bytree=1.0,
                        objective='binary:logistic', nthread=7, scale_pos_weight=1, seed=27)
    parms = {'base_score': 0.5, 'booster': 'gbtree', 'colsample_bylevel': 1, 
     'colsample_bytree': 1.0, 'gamma': 0.2, 'learning_rate': 0.01, 
     'max_delta_step': 0, 'max_depth': 20, 'min_child_weight': 2, 
     'missing': None, 'n_estimators': 9001, 'nthread': 7, 
     'objective': 'binary:logistic', 'reg_alpha': 0, 'reg_lambda': 1, 
     'scale_pos_weight': 1, 'seed': 27, 'silent': True, 
     'subsample': 0.2, 'verbosity': 0}
#     alg = XGBClassifier(learning_rate=0.05, n_estimators=1400, max_depth=20,
#                         min_child_weight=3, gamma=0.2, subsample=0.6, colsample_bytree=1.0,
#                         objective=logregobjective, nthread=7, scale_pos_weight=1, seed=27)

    if useTrainCV:
        print("Start Feeding Data")
        xgb_param = alg.get_xgb_params()
        print(alg.get_xgb_params())
        xgtrain = xgb.DMatrix(X_train.values, label=y_train.values)
        # xgtest = xgb.DMatrix(X_test.values, label=y_test.values)
        print('running cross validation')
        cvresult = xgb.cv(xgb_param, xgtrain, num_boost_round=alg.get_params()['n_estimators'], nfold=cv_folds,
                          verbose_eval=1000, stratified=True,
                          feval=cupEvalScore_negative, maximize=False)
        alg.set_params(n_estimators=cvresult.shape[0])
        print(alg.get_xgb_params())
        print('Best number of trees = {}\n'.format(cvresult.shape[0]))
#         print(cvresult)

    # 建模
    print('Start Training')
    alg.fit(X_train, y_train,
            eval_set=[(X_train, y_train), (X_test, y_test)],
            eval_metric=cupEvalScore_negative, verbose=1000)

    # 对训练集预测
    print("Start Predicting")
    predictions = alg.predict(X_test)
    pred_proba = alg.predict_proba(X_test)[:, 1]

    # 输出模型的一些结果
    print("\n关于现在这个模型")
    print("准确率 : %.4g" % metrics.accuracy_score(y_test, predictions))
    print("AUC 得分 (训练集): %f" % metrics.roc_auc_score(y_test, pred_proba))
    print("F1 Score 得分 (训练集): %f" % metrics.f1_score(y_test, predictions))
    matrix = metrics.confusion_matrix(y_test, predictions)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
    totalArray = np.reshape(matrix, 4)
    print(f"Score Cup: {weightSum}")
    print(f"Confusion matrix: \n{matrix}")
    print(f"Percentage of positives: \n{100*np.sum(totalArray*np.array([0,1,0,1]))/np.sum(totalArray)}")
    print(f"Actual positives: \n{100*np.sum(totalArray*np.array([0,0,1,1]))/np.sum(totalArray)}")

    feat_imp = alg.feature_importances_
    feat = X_train.columns.tolist()
    # clf.best_estimator_.booster().get_fscore()
    res_df = pd.DataFrame({'Features': feat, 'Importance': feat_imp}).sort_values(by='Importance', ascending=False)
    res_df.plot('Features', 'Importance', kind='bar', title='Feature Importances')
    plt.ylabel('Feature Importance Score')
    plt.show()
    print(res_df)
    print(res_df["Features"].tolist())


# In[ ]:


if __name__ == "__main__":
    # logistic_regression()
    # randomForest()
    # logistic_with_smote()
    # neural_nets()
    start = time.time()
    X_train, X_test, y_train, y_test = data_processor()
    xgb_classifier(X_train, X_test, y_train, y_test)
#     randomForest()
#     neural_nets()
#     logistic_regression()
#     logistic_with_smote()


print("Total Time is: ", (time.time() - start)/60)


# In[ ]:





# In[88]:


def huber_approx_obj(preds, dtrain):
    d = preds - dtrain.get_labels() #remove .get_labels() for sklearn
    h = 1  #h is delta in the graphic
    scale = 1 + (d / h) ** 2
    scale_sqrt = np.sqrt(scale)
    grad = d / scale_sqrt
    hess = 1 / scale / scale_sqrt
    return grad, hess

def huber_approx_obj(preds, dtrain):
    d = preds - dtrain.get_labels() #remove .get_labels() for sklearn
    h = 1  #h is delta in the graphic
    scale = 1 + (d / h) ** 2
    scale_sqrt = np.sqrt(scale)
    grad = d / scale_sqrt
    hess = 1 / scale / scale_sqrt
    return grad, hess

def huber_approx_objective(preds, dtrain):
    d = preds - dtrain #remove .get_labels() for sklearn
    h = 1  #h is delta in the graphic
    scale = 1 + (d / h) ** 2
    scale_sqrt = np.sqrt(scale)
    grad = d / scale_sqrt
    hess = 1 / scale / scale_sqrt
    return grad, hess


def fair_obj(preds, dtrain):
    """y = c * abs(x) - c**2 * np.log(abs(x)/c + 1)"""
    x = preds - dtrain.get_labels()
    c = 1
    den = abs(x) + c
    grad = c*x / den
    hess = c*c / den ** 2
    return grad, hess

def fair_objective(preds, dtrain):
    """y = c * abs(x) - c**2 * np.log(abs(x)/c + 1)"""
    x = preds - dtrain
    c = 1
    den = abs(x) + c
    grad = c*x / den
    hess = c*c / den ** 2
    return grad, hess


def log_cosh_obj(preds, dtrain):
    x = preds - dtrain.get_labels()
    grad = np.tanh(x)
    hess = 1 / np.cosh(x)**2
    return grad, hess

def log_cosh_objective(preds, dtrain):
    x = preds - dtrain
    grad = np.tanh(x)
    hess = 1 / np.cosh(x)**2
    return grad, hess

def logregobj(preds, dtrain):
    labels = dtrain.get_label()
    preds = 1.0 / (1.0 + np.exp(-preds))
    grad = preds - labels
    hess = preds * (1.0 - preds)
    return grad, hess

def logregobjective(y_true, y_pred):
    labels = y_true
    y_pred = 1.0 / (1.0 + np.exp(-y_pred))
    grad = y_pred - labels
    hess = y_pred * (1.0 - y_pred)
    return grad, hess


def cupScoreObj(y_true, y_pred):
    grad = 20*(y_true**2) - 35*y_true + 50*y_pred
    hess = 50 * (y_pred**0)
    return grad, hess

def evalerror2(preds, dtrain):
    labels = dtrain
    return 'error', float(sum(labels != (preds > 0.0))) / len(labels)

# def evalerror(preds, dtrain):
#     labels = dtrain.get_label()
#     return 'error', float(sum(labels != (preds > 0.0))) / len(labels)

def evalerror(y_predicted, y_true):
    labels = y_true.get_label()
    y_predicted = np.round(y_predicted)
    matrix = metrics.confusion_matrix(labels, y_predicted)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
    return 'cupScore', weightSum

def evalerror_negative(y_predicted, y_true):
    labels = y_true.get_label()
    y_predicted = np.round(y_predicted)
    matrix = metrics.confusion_matrix(labels, y_predicted)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,25,5,-5]))
    return 'cupScore', weightSum


# In[35]:




def logistic_regression():
    """
    F1 score is: 0.7285714285714285
    AUC Score is: 0.9667565771367231
    """
    X_train, X_test, y_train, y_test = data_processor()
    clf = LogisticRegression(C=1e5)
    clf.fit(X_train, y_train)

    print("Score: ", clf.score(X_test, y_test))
    y_pred = clf.predict(X_test)
    y_pred_proba = clf.predict_proba(X_test)[:, 1]
    print("F1 score is: {}".format(f1_score(y_test, y_pred)))
    print("AUC Score is: {}".format(roc_auc_score(y_test, y_pred_proba)))
    matrix = metrics.confusion_matrix(y_test, y_pred)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
    print(f"Score Cup: {weightSum}")
    print(f"Confusion matrix: \n{matrix}")


def logistic_with_smote():
    X_train, X_test, y_train, y_test = data_processor()

    clf = LogisticRegression(C=1e5)
    clf.fit(X_train, y_train)
    # build model with SMOTE imblearn
    smote_pipeline = make_pipeline_imb(SMOTE(random_state=4), clf)

    smote_model = smote_pipeline.fit(X_train, y_train)
    smote_prediction = smote_model.predict(X_test)
    smote_prediction_proba = smote_model.predict_proba(X_test)[:, 1]

    print(classification_report_imbalanced(y_test, smote_prediction))
    print('SMOTE Pipeline Score {}'.format(smote_pipeline.score(X_test, y_test)))
    print("SMOTE AUC score: ", roc_auc_score(y_test, smote_prediction_proba))
    print("SMOTE F1 Score: ", f1_score(y_test, smote_prediction))
    matrix = metrics.confusion_matrix(y_test, smote_prediction)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
    print(f"Score Cup: {weightSum}")
    print(f"Confusion matrix: \n{matrix}")


def randomForest():
    """
    F1 score is: 0.7857142857142857
    AUC Score is: 0.9450972761670293
    """
    X_train, X_test, y_train, y_test = data_processor()
    # parameters = {'n_estimators': [10, 20, 30, 50], 'max_depth': [2, 3, 4]}

    clf = RandomForestClassifier(max_depth=4, n_estimators=20)
    # clf = GridSearchCV(alg, parameters, n_jobs=4)
    clf.fit(X_train, y_train)
    print("Score: ", clf.score(X_test, y_test))
    y_pred = clf.predict(X_test)
    y_pred_proba = clf.predict_proba(X_test)[:, 1]

    print("F1 score is: {}".format(f1_score(y_test, y_pred)))
    print("AUC Score is: {}".format(roc_auc_score(y_test, y_pred_proba)))
    matrix = metrics.confusion_matrix(y_test, y_pred)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
    print(f"Score Cup: {weightSum}")
    print(f"Confusion matrix: \n{matrix}")

    # print("The Features Importance are: ")  # for feature, value in zip(X_train.columns, clf.feature_importances_):
    #     print(feature, value)
    # print(clf.best_estimator_)
    # print(clf.best_params_)
    # print(clf.best_score_)


def neural_nets():
    """
    Score:  0.9994148145547324
    F1 score is: 0.822695035460993
    AUC Score is: 0.9608730286337007
    """
    X_train, X_test, y_train, y_test = data_processor()
    clf = MLPClassifier(hidden_layer_sizes=(100, 100, 100,))

    clf.fit(X_train, y_train)
    print("Score: ", clf.score(X_test, y_test))
    y_pred = clf.predict(X_test)
    y_pred_proba = clf.predict_proba(X_test)[:, 1]
    print("F1 score is: {}".format(f1_score(y_test, y_pred)))
    print("AUC Score is: {}".format(roc_auc_score(y_test, y_pred_proba)))
    matrix = metrics.confusion_matrix(y_test, y_pred)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
    print(f"Score Cup: {weightSum}")
    print(f"Confusion matrix: \n{matrix}")


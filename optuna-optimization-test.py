
# coding: utf-8

# # XGBoost Optimization for DMC 2019 using Optuna
# 
# Based on the python scripts from https://github.com/pfnet/optuna/blob/master/examples/pruning/xgboost_integration.py and also optuna API reference https://optuna.readthedocs.io/en/stable/reference/index.html

# In[143]:


from __future__ import division

import numpy as np
import pandas as pd

import sklearn.datasets
import sklearn.metrics as metrics
from sklearn import metrics
from sklearn.metrics import f1_score, roc_auc_score, roc_curve, auc
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, RobustScaler

import xgboost as xgb
from xgboost import XGBClassifier

import optuna
from bayes_opt import BayesianOptimization

from itertools import chain
import sys


# Carregando os dados do problema

# In[89]:


df = pd.read_csv('Datasets/train.csv', sep='|')
dall = xgb.DMatrix(df[df.columns[:-1].tolist()], label=pd.DataFrame(df[df.columns[-1]]))

train1 = pd.read_csv('Datasets/random_state_42/fold1/trn1.csv' , sep='|', index_col=0)
test1 = pd.read_csv('Datasets/random_state_42/fold1/tst1.csv', sep='|', index_col=0)

train2 = pd.read_csv('Datasets/random_state_42/fold2/trn2.csv', sep='|', index_col=0)
test2 = pd.read_csv('Datasets/random_state_42/fold2/tst2.csv', sep='|', index_col=0)

train3 = pd.read_csv('Datasets/random_state_42/fold3/trn3.csv', sep='|', index_col=0)
test3 = pd.read_csv('Datasets/random_state_42/fold3/tst3.csv', sep='|', index_col=0)


dtrain1 = xgb.DMatrix(train1[train1.columns[:-1].tolist()], label=pd.DataFrame(train1[train1.columns[-1]]))
dtest1 = xgb.DMatrix(test1[test1.columns[:-1].tolist()], label=pd.DataFrame(test1[test1.columns[-1]]))

dtrain2 = xgb.DMatrix(train2[train2.columns[:-1].tolist()], label=pd.DataFrame(train2[train2.columns[-1]]))
dtest2 = xgb.DMatrix(test2[test2.columns[:-1].tolist()], label=pd.DataFrame(test2[test2.columns[-1]]))

dtrain3 = xgb.DMatrix(train3[train3.columns[:-1].tolist()], label=pd.DataFrame(train3[train3.columns[-1]]))
dtest3 = xgb.DMatrix(test3[test3.columns[:-1].tolist()], label=pd.DataFrame(test3[test3.columns[-1]]))


# Funções auxiliares para determinar a qualidade do classificador para o otimizador.

# In[90]:


def cupEvalScore(y_predicted, y_true):
    labels = y_true.get_label()
    y_predicted = np.round(y_predicted)
    matrix = metrics.confusion_matrix(labels, y_predicted)
#     print(matrix)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
    return 'cupScore', weightSum

def score_cup(y_tst , y_pred):
    matrix = metrics.confusion_matrix(y_tst, y_pred)
    return np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))

def best_threshold(y_tst, y_prob, incremento=0.1):
    threshold=incremento
    n_iter = np.linspace(0.0, 1.0, (1.0/incremento))
    scor = -9999999
    thr_start = 0.0
    thr_end= 0.0
    thr_set = False
#     print('11111')
    for n in n_iter:
#         print('2222')
        y_pred = (y_prob >= threshold).astype(int)
#         print('3333')
        sc = score_cup(y_tst, y_pred)
        if (sc > scor):
            scor = sc
            thr_start = threshold
            thr_end = threshold
            thr_set == True
        elif (sc == scor and (thr_set == True)):
            thr_end = threshold
        else:
            thr_set = False
        threshold+= incremento
    thr = (thr_start+thr_end)/2.0
    return thr, scor

def get_full_array(y1, y2, y3):
    fullarray = list(chain(y1,y2,y3))
    return np.array(fullarray)


# Função objetivo (a ser minimizada) do otimizador optuna.

# In[138]:


def xgb_evaluate_f1(trial):
    param = {'booster': 'dart', # o tipo do booster deve ser configurado manualmente aqui em cima
             # General Parameters
             'silent': 1,
#              'booster':  trial.suggest_categorical('booster', ['gbtree', 'gblinear', 'dart']), 
             'verbosity': 0,
             'nthread': 7,
             'n_jobs': 7,
             'disable_default_eval_metric': 0, # Flag to disable default metric. Set to >0 to disable.
                
             # Parameters for Tree Booster
            #  'eta': eta, # alias learning_rate
            #  'learning_rate': eta, # alias learning_rate
            #  'gamma': gamma, # alias min_split_loss
            #  'min_split_loss': gamma, # alias min_split_loss
            #  'max_depth': int(max_depth),
            #  'min_child_weight': min_child_weight,
            #  'max_delta_step': max_delta_step,
            #  'subsample': subsample,
            #  'colsample_bytree': colsample_bytree,
            #  'colsample_bylevel': colsample_bylevel,
            #  'colsample_bynode': colsample_bynode,
             'lambda': trial.suggest_loguniform('lambda', 1e-8, 1.0),
            #  'reg_lambda': lmbd,
             'alpha': trial.suggest_loguniform('alpha', 1e-8, 1.0),
            #  'reg_alpha': alpha, 
#              'tree_method': 'auto',

#              'scale_pos_weight': trial.suggest_uniform('scale_pos_weight', 1e-2, 40.0),
#              'updater': 'grow_colmaker,prune',
#              'refresh_leaf': 1,
#              'process_type': 'default',
            #  'grow_policy': 'depthwise',

             # Additional parameters for Dart Booster (booster=dart)
            #  'sample_type': 'uniform',
            #  'normalize_type': 'forest',
            #  'rate_drop': rate_drop,
#              'one_drop': 0,
            #  'skip_drop': skip_drop,

             # Learning Task Parameters
             'objective': 'binary:logistic',
             'base_score': 0.5,
             # User can add multiple evaluation metrics. 
             # Python users: remember to pass the metrics in as list of 
             # parameters pairs instead of map, so that latter eval_metric won't override previous one
             'eval_metric':'aucpr', 
             'seed': 0,
             'n_estimators': trial.suggest_int('n_estimators', 100, 2000)
            }
    param['reg_lambda'] = param['lambda']
    param['reg_alpha'] = param['alpha']
    if param['booster'] == 'gbtree' or param['booster'] == 'dart':
        param['eta'] = trial.suggest_loguniform('eta', 1e-3, 0.5)
        param['learning_rate'] = param['eta']
        param['gamma'] = trial.suggest_loguniform('gamma', 1e-8, 1.0)
        param['min_split_loss'] = param['gamma']
        param['max_depth'] = trial.suggest_int('max_depth', 2, 50)
        param['min_child_weight'] = trial.suggest_uniform('min_child_weight', 1, 20)
        param['max_delta_step'] = trial.suggest_uniform('max_delta_step', 0.0, 100.0)
        param['subsample'] = trial.suggest_discrete_uniform('subsample', 0.00, 1.00, 0.05)
        param['colsample_bytree'] = trial.suggest_discrete_uniform('colsample_bytree', 0.00, 1.00, 0.05)
        param['colsample_bylevel'] = trial.suggest_discrete_uniform('colsample_bylevel', 0.00, 1.00, 0.05)
        param['colsample_bynode'] = trial.suggest_discrete_uniform('colsample_bynode', 0.00, 1.00, 0.05)
        param['tree_method'] = 'auto'
        param['scale_pos_weight'] = trial.suggest_discrete_uniform('scale_pos_weight', 0.00, 50.00, 0.5)
        param['updater'] = 'grow_colmaker,prune'
        param['refresh_leaf'] = 1
        param['process_type'] = 'default'
        param['grow_policy'] = trial.suggest_categorical('grow_policy', ['depthwise', 'lossguide'])
    if param['booster'] == 'dart':
        param['sample_type'] = trial.suggest_categorical('sample_type', ['uniform', 'weighted'])
        param['normalize_type'] = trial.suggest_categorical('normalize_type', ['tree', 'forest'])
        param['rate_drop'] = trial.suggest_loguniform('rate_drop', 1e-2, 1.0)
        param['one_drop'] = 0
        param['skip_drop'] = trial.suggest_loguniform('skip_drop', 1e-1, 1.0)

    bst1 = xgb.train(param, dtrain1, 
                     evals=[(dtest1, 'val'), (dtrain1, 'trn')], num_boost_round=param['n_estimators'],
                     verbose_eval=param['n_estimators'])
    bst2 = xgb.train(param, dtrain2, 
                     evals=[(dtest2, 'val'), (dtrain2, 'trn')], num_boost_round=param['n_estimators'],
                     verbose_eval=param['n_estimators'])
    bst3 = xgb.train(param, dtrain3, 
                     evals=[(dtest3, 'val'), (dtrain3, 'trn')], num_boost_round=param['n_estimators'],
                     verbose_eval=param['n_estimators'])
    
    preds1 = bst1.predict(dtest1)
    y_true1 = test1[test1.columns[-1]]
    best_thr1, best_score1 = best_threshold(y_true1, preds1, 0.001)
    
    preds2 = bst2.predict(dtest2)
    y_true2 = test2[test2.columns[-1]]
    best_thr2, best_score2 = best_threshold(y_true2, preds2, 0.001)
    
    preds3 = bst3.predict(dtest3)
    y_true3 = test3[test3.columns[-1]]
    best_thr3, best_score3 = best_threshold(y_true3, preds3, 0.001)

    
    y_prob = get_full_array(preds1, preds2, preds3)
    y_test = get_full_array(y_true1, y_true2, y_true3)
    best_thr, best_score = best_threshold(y_test, y_prob, 0.001)
    
    pred_labels1 = (preds1>=best_thr).astype(int)
    aucpr1 = metrics.roc_auc_score(y_true1, pred_labels1)
    f1_1 = metrics.f1_score(y_true1, pred_labels1)
        
    pred_labels2 = (preds2>=best_thr).astype(int)
    aucpr2 = metrics.roc_auc_score(y_true2, pred_labels2)
    f1_2 = metrics.f1_score(y_true2, pred_labels2)

    pred_labels3 = (preds3>=best_thr).astype(int)
    aucpr3 = metrics.roc_auc_score(y_true3, pred_labels3)
    f1_3 = metrics.f1_score(y_true3, pred_labels3)
    
    cup_score1 = score_cup(y_true1, pred_labels1)
    cup_score2 = score_cup(y_true2, pred_labels2)
    cup_score3 = score_cup(y_true3, pred_labels3)
    
    f1_score = ((f1_1+f1_2+f1_3)/3.0)
    aucpr = ((aucpr1 + aucpr2 + aucpr3)/3.0)
    
    trial.set_user_attr('booster', str(param['booster']))

    trial.set_user_attr('Best_Cup_Score', str(best_score))
    trial.set_user_attr('Best_Threshold', str(best_thr))
    trial.set_user_attr('F1-Score', str(f1_score))
    trial.set_user_attr('AUC_PR', str(aucpr))
    
    trial.set_user_attr('F1-Score_fold1', str(f1_1))
    trial.set_user_attr('F1-Score_fold2', str(f1_2))
    trial.set_user_attr('F1-Score_fold3', str(f1_3))
    trial.set_user_attr('AUC_PR_fold1', str(aucpr1))
    trial.set_user_attr('AUC_PR_fold2', str(aucpr2))
    trial.set_user_attr('AUC_PR_fold3', str(aucpr3))
    trial.set_user_attr('Cup_Score_fold1', str(cup_score1))
    trial.set_user_attr('Cup_Score_fold2', str(cup_score2))
    trial.set_user_attr('Cup_Score_fold3', str(cup_score3))
    
    
    trial.set_user_attr('Best_Cup_Score_fold1', str(best_score1)) 
    trial.set_user_attr('Best_Cup_Score_fold2', str(best_score2)) 
    trial.set_user_attr('Best_Cup_Score_fold3', str(best_score3))
    trial.set_user_attr('Best_Threshold_fold1', str(best_thr1))
    trial.set_user_attr('Best_Threshold_fold2', str(best_thr2))
    trial.set_user_attr('Best_Threshold_fold3', str(best_thr3))
    
    print('Best Cup Score:', best_score)
    print('Best Threshold: ', best_thr)
    print('F1-Score: ', f1_score)
    print('AUC_PR: ', aucpr)
    print('Scores: ', cup_score1 , cup_score2, cup_score3)
#     print('Scores: ', cupEvalScore(pred_labels1, y_true1), 
#           cupEvalScore(pred_labels2, y_true2), cupEvalScore(pred_labels3, y_true3))
    print('F1-Scores: ', f1_1, f1_2, f1_3)
    print('AUC_PRs: ', aucpr1, aucpr2, aucpr3)
    print('Best thresholds: ', best_thr1, best_thr2, best_thr3)
    print('Best scores: ', best_score1, best_score2, best_score3)
    return (1.0 - f1_score)


# Configurando um "estudo de caso" do optuna com armazenamento em sqlite local.
# 
# Se esse estudo já existe ele vai ser carregado para continuação da otimização.

# In[139]:


study_name = 'dmc2019-ruan-study-random-state-42-v2'  # Unique identifier of the study.
study = optuna.create_study(study_name=study_name,
                            storage='sqlite:///ruan-study-random-state-42-v2.db',
                            load_if_exists=True)


# Chamada da otimização de fato, deve ser definido o número de trials n_trials e um tempo máximo em segundos para finalização automática.

# In[140]:


study.optimize(xgb_evaluate_f1, n_trials=10, timeout=3600)


# Para visualização de todas as rodadas do estudo de caso.

# In[134]:


study.trials_dataframe()


# Visualização de somente a melhor rodada (menor função objetivo):

# In[141]:


print(study.best_trial)

# Somente os parâmetros:
# study.best_trial.params

# Somente os atributos do usuário:
# study.best_trial.user_attrs



# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import seaborn as sns
import warnings
import matplotlib.pyplot as plt
warnings.filterwarnings('ignore')


# In[2]:


train = pd.read_csv('Datasets/train.csv', sep='|')


# In[5]:


train.info()


# In[7]:


train.describe()


# In[12]:


def feature_vs_target(feature):
    g  = sns.factorplot(x=feature,y="fraud",data=train,kind="bar", size = 6 , palette = "muted")
    g.despine(left=True)
    g = g.set_ylabels("fraud probability")
    
def fdp_by_target(feature):
    g = sns.kdeplot(train[feature][(train["fraud"] == 0)], color="Red", shade = True)
    g = sns.kdeplot(train[feature][(train["fraud"] == 1)], ax =g, color="Blue", shade= True)
    g.set_xlabel(feature)
    g.set_ylabel("Frequency")
    g = g.legend(["Not Fraud","Fraud"])

def fdp_log_transform(feature):
    transformed = train[feature].map(lambda x: np.log(x) if x > 0 else 0)
    g = sns.distplot(transformed, color="m", label="Skewness : %.2f"%(transformed.skew()))
    g = g.legend(loc="best")

def skewness_feature(feature):
    g = sns.distplot(train[feature], color="m", label="Skewness : %.2f"%(train[feature].skew()))
    g = g.legend(loc="best")

def plot_feature_distribution(feature):
    g = sns.kdeplot(train[feature], color="Red", shade = True)
    
def plot_all_fpd(df):
    fig, ax = plt.subplots(3, 3, figsize=(18,4))
    columns = df.drop('fraud', 1).columns.tolist()
    for i in range(3):
        for j in range(3):
            feature = columns.pop()
            values = df[feature].values
            sns.distplot(values, ax=ax[i,j])
            ax[i,j].set_title(f'Distribution of {feature}', fontsize=14)
            #ax[i,j].set_xlim([min(values), max(values)])


# In[9]:


#categoricas{trustLevel,  lineItemVoids, scansWithoutRegistration, quantityModifications}
#normais{totalScanTimeInSeconds, grandTotal}
#enviesadas{scannedLineItemsPerSecond 	valuePerSecond 	lineItemVoidsPerPosition}


# In[13]:


sns.countplot(train['fraud'], palette='Set3')
print("There are {}% target values with 1".format(100 * train["fraud"].value_counts()[1]/train.shape[0]))


# ### Features "Categóricas" com preservação da ordem

# In[22]:


feature_vs_target('trustLevel')


# In[23]:


feature_vs_target('lineItemVoids')


# In[24]:


feature_vs_target('scansWithoutRegistration')


# In[25]:


feature_vs_target('quantityModifications')


# ### Features com distribuição normal

# In[32]:


fdp_by_target('totalScanTimeInSeconds')


# In[40]:


plot_feature_distribution('totalScanTimeInSeconds')


# In[36]:


fdp_by_target('grandTotal')


# In[37]:


plot_feature_distribution('grandTotal')


# ### Features enviesadas
# - Aplicar transformação com log para evitar overweight

# In[45]:


#scannedLineItemsPerSecond 	valuePerSecond 	lineItemVoidsPerPosition
skewness_feature('scannedLineItemsPerSecond')


# In[46]:


skewness_feature('valuePerSecond')


# In[47]:


skewness_feature('lineItemVoidsPerPosition')


# ### Transformação com log

# In[50]:


fdp_log_transform('scannedLineItemsPerSecond')


# In[51]:


fdp_log_transform('valuePerSecond')


# In[52]:


fdp_log_transform('lineItemVoidsPerPosition')


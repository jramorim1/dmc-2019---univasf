
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import seaborn as sns
import warnings
import matplotlib.pyplot as plt
warnings.filterwarnings('ignore')


# In[2]:


train = pd.read_csv('Datasets/train.csv', sep='|')
test = pd.read_csv('Datasets/test.csv', sep='|')


# In[3]:


train.info()


# In[3]:


train.describe()


# In[4]:


test.describe()


# In[26]:


def feature_vs_target(feature):
    g  = sns.factorplot(x=feature,y="fraud",data=train,kind="bar", size = 6 , palette = "muted")
    g.despine(left=True)
    g = g.set_ylabels("fraud probability")
    
def fdp_by_target(feature):
    g = sns.kdeplot(train[feature][(train["fraud"] == 0)], color="Red", shade = True)
    g = sns.kdeplot(train[feature][(train["fraud"] == 1)], ax =g, color="Blue", shade= True)
    g.set_xlabel(feature)
    g.set_ylabel("Frequency")
    g = g.legend(["Not Fraud","Fraud"])

def fdp_log_transform(feature):
    transformed = train[feature].map(lambda x: np.log(x) if x > 0 else 0)
    g = sns.distplot(transformed, color="r", label="Skewness train : %.2f"%(transformed.skew()))
    g = g.legend(loc="best")
    
    transformed2 = test[feature].map(lambda x: np.log(x) if x > 0 else 0)
    g2 = sns.distplot(transformed2, color="b", label="Skewness test : %.2f"%(transformed2.skew()))
    g2 = g2.legend(loc="best")

def skewness_feature(feature):
    g = sns.distplot(train[feature], color="r", label="Skewness train : %.2f"%(train[feature].skew()))
    g = g.legend(loc="best")
    
    g2 = sns.distplot(test[feature], color="b", label="Skewness test : %.2f"%(test[feature].skew()))
    g2 = g2.legend(loc="best")

def plot_feature_distribution(feature):
    g = sns.kdeplot(train[feature], color="Red", shade = True)
    g2 = sns.kdeplot(test[feature], color="Blue", shade = True)
    
def plot_all_fpd(df):
    fig, ax = plt.subplots(3, 3, figsize=(18,4))
    columns = df.columns.tolist()
    for i in range(3):
        for j in range(3):
            feature = columns.pop()
            values = df[feature].values
            sns.distplot(values, ax=ax[i,j])
            ax[i,j].set_title(f'Distribution of {feature}', fontsize=14)
            #ax[i,j].set_xlim([min(values), max(values)])


# In[6]:


#categoricas{trustLevel,  lineItemVoids, scansWithoutRegistration, quantityModifications}
#normais{totalScanTimeInSeconds, grandTotal}
#enviesadas{scannedLineItemsPerSecond 	valuePerSecond 	lineItemVoidsPerPosition}


# In[7]:


sns.countplot(train['fraud'], palette='Set3')
print("There are {}% target values with 1".format(100 * train["fraud"].value_counts()[1]/train.shape[0]))


# ### Features "Categóricas" com preservação da ordem

# In[8]:


feature_vs_target('trustLevel')


# In[9]:


feature_vs_target('lineItemVoids')


# In[10]:


feature_vs_target('scansWithoutRegistration')


# In[11]:


feature_vs_target('quantityModifications')


# ### Features com distribuição normal

# In[12]:


fdp_by_target('totalScanTimeInSeconds')


# In[13]:


plot_feature_distribution('totalScanTimeInSeconds')


# In[14]:


fdp_by_target('grandTotal')


# In[15]:


plot_feature_distribution('grandTotal')


# ### Features enviesadas
# - Aplicar transformação com log para evitar overweight

# In[30]:


train.columns


# In[31]:


skewness_feature('trustLevel')


# In[35]:


skewness_feature('valuePerSecond')


# In[27]:


#scannedLineItemsPerSecond 	valuePerSecond 	lineItemVoidsPerPosition
skewness_feature('scannedLineItemsPerSecond')


# In[28]:


skewness_feature('valuePerSecond')


# In[29]:


skewness_feature('lineItemVoidsPerPosition')


# ### Transformação com log

# In[15]:


fdp_log_transform('scannedLineItemsPerSecond')


# In[16]:


fdp_log_transform('valuePerSecond')


# In[23]:


fdp_log_transform('lineItemVoidsPerPosition')


# In[19]:


plot_all_fpd(train)


# In[22]:


plot_all_fpd(test)


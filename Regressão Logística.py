
# coding: utf-8

# In[16]:


import pandas as pd
import numpy as np

from sklearn import metrics

train = pd.read_csv('Datasets/train.csv', sep='|')

# b_0 = Constante = -2.125
# x_1 = Trust Level
# x_2= TotalScanTimeInSeconds 
# x_3  = GrandTotal 
# x_4 = LineItemVoids 
# x_5 = ScansWithoutRegistration 
# x_6 = LineItemVoidsPerPosition
features_to_select = ['trustLevel', 'totalScanTimeInSeconds', 'grandTotal', 'lineItemVoids',
                          'scansWithoutRegistration','lineItemVoidsPerPosition', 'fraud']
train = train.loc[:,features_to_select]

train.head()


# In[17]:


train['f_x_i'] =  -2.125-6.008*train['trustLevel']+0.004*train['totalScanTimeInSeconds']
train['f_x_i'] = train['f_x_i'] +0.016*train['grandTotal']+3.020*train['lineItemVoids']
train['f_x_i'] = train['f_x_i'] +0.466*train['scansWithoutRegistration']-62.226*train['lineItemVoidsPerPosition'] 




train['P_Y_i'] = 1.0/(1+np.exp(-train['f_x_i']))

train['P_fraud'] = round(train['P_Y_i'] )


# In[22]:


train.head(10)


matrix = metrics.confusion_matrix(train['fraud'], train['P_fraud'])
weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
print(f"Score Cup: {weightSum}")
auc = metrics.roc_auc_score(train['fraud'], train['P_fraud'], average=None)
print(f"AUC: {auc*100}%")

print(f"Confusion matrix: \n{matrix}")


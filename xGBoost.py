
# coding: utf-8

# In[3]:


import pandas as pd
import numpy as np

import xgboost as xgb
from xgboost import XGBClassifier

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, RobustScaler
from sklearn import metrics
from sklearn.metrics import f1_score, roc_auc_score, roc_curve, auc

from bayes_opt import BayesianOptimization


# ## Data Processing

# Normalização - min e max (percentil 99)
#     N = Max - val / max - min
# 
# Teste KS - Definir ponto de corte
# 
# No crossvalidation:
# * Dividir em 3 estratificada - já fiz.
# * Pegar somente o score positivo de cada classe
# * Plotar o KS dele
#     * Percentual acumulado da classe positiva, e da positiva
#     * Ponto de corte das classes
# * Plotar função custo da competição       

# In[4]:


def data_processor(oversampling=False, random=0):
    df = pd.read_csv('Datasets/train.csv', sep='|')
    features_to_select = ['trustLevel', 'totalScanTimeInSeconds', 'grandTotal', 'lineItemVoids',
                          'scansWithoutRegistration', 'quantityModifications', 
                          'scannedLineItemsPerSecond', 'valuePerSecond', 'lineItemVoidsPerPosition']

#     print('Use StandardScaler to process the column data')
#     scaler = StandardScaler()
#     scaler = RobustScaler()
#     df[df.columns[:-1].tolist()] = scaler.fit_transform(df[df.columns[:-1].tolist()])
    # print(df.head(5))
    X = df[df.columns[:-1].tolist()]
    X = X[features_to_select]
    y = df[df.columns[-1]]

    print("Train Test Split ratio is 0.33")
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.333333, random_state=random, stratify=y)
    
    if (oversampling == True): # Baseado em https://www.kaggle.com/rafjaa/resampling-strategies-for-imbalanced-datasets
        df_train = X_train.join(y_train) # .reset_index(drop=True)
        # Class count
        count_class_0, count_class_1 = df_train.fraud.value_counts()

        # Divide by class
        df_class_0 = df_train[df_train['fraud'] == 0]
        df_class_1 = df_train[df_train['fraud'] == 1]

        df_class_1_over = df_class_1.sample(count_class_0, replace=True)
        df_test_over = pd.concat([df_class_0, df_class_1_over], axis=0)

        print('Random over-sampling:')
        print(df_test_over.fraud.value_counts())

        df_test_over.fraud.value_counts().plot(kind='bar', title='Count (fraud)')   

        X_train = df_test_over[features_to_select]
        y_train = df_test_over[df_test_over.columns[-1]]
    
    print(f"X_train shape: {X_train.shape}")
    print(f"y_train shape: {y_train.shape}")
    print(f"True class percentage in train set: {100*y_train.sum()/y_train.count()}%")
    
    print(f"X_test shape: {X_test.shape}")
    print(f"y_test shape: {y_test.shape}")
    print(f"True class percentage in test set: {100*y_test.sum()/y_test.count()}%")

    return X_train, X_test, y_train, y_test

X_train, X_test, y_train, y_test = data_processor()

dtrain = xgb.DMatrix(X_train, label=pd.DataFrame(y_train))
dtest = xgb.DMatrix(X_test, label=pd.DataFrame(y_test))

df = pd.read_csv('Datasets/train.csv', sep='|')
dall = xgb.DMatrix(df[df.columns[:-1].tolist()], label=pd.DataFrame(df[df.columns[-1]]))

dtrain.save_binary('train.buffer')
dtest.save_binary('test.buffer')


# ## Setting Parameters

# In[5]:


# XGBoost can use either a list of pairs or a dictionary to set parameters. For instance:
# Booster parameters - https://github.com/dmlc/xgboost/blob/master/doc/parameter.rst
param = {'booster': 'gbtree', # General Parameters
         'silent': 1,
         'verbosity': 1,
         'nthread': 7,
         'disable_default_eval_metric': 1, # Flag to disable default metric. Set to >0 to disable.
         
         # Parameters for Tree Booster
         'eta': 0.3, # alias learning_rate
         'gamma': 0, # alias min_split_loss
         'max_depth': 6,
         'min_child_weight': 1,
         'max_delta_step': 0,
         'subsample': 1,
         'colsample_bytree': 1,
         'colsample_bylevel': 1,
         'colsample_bynode': 1,
         'lambda': 1,
         'alpha': 0,
         'tree_method': 'auto',
         'scale_pos_weight': 1,
         'updater': 'grow_colmaker,prune',
         'refresh_leaf': 1,
         'process_type': 'default',
         'grow_policy': 'depthwise',
         
         # Additional parameters for Dart Booster (booster=dart)
         'sample_type': 'uniform',
         'normalize_type': 'tree',
         'rate_drop': 0.0,
         'one_drop': 0,
         'skip_drop': 0.0,
         
         # Learning Task Parameters
         'objective': 'binary:logistic',
         'base_score': 0.5,
         # User can add multiple evaluation metrics. 
         # Python users: remember to pass the metrics in as list of 
         # parameters pairs instead of map, so that latter eval_metric won't override previous one
#          'eval_metric':'aucpr', 
         'seed': 0
        } 
                
# Specify validations set to watch performance
evallist = [(dtest, 'eval'), (dtrain, 'train')]


# In[119]:


print(params)

# |   iter    |  target   | colsam... | colsam... | colsam... |    eta    |   gamma   | max_de... | max_depth | min_ch... | subsample |
# |  139      |  0.9347   |  0.9      |  0.9      |  0.9      |  0.1      |  1.0      |  2.941    |  2.0      |  1.0      |  0.9      |


# In[6]:


# XGBoost can use either a list of pairs or a dictionary to set parameters. For instance:
# Booster parameters - https://github.com/dmlc/xgboost/blob/master/doc/parameter.rst
params = {'booster': 'dart', # General Parameters
         'silent': 1,
         'verbosity': 1,
         'nthread': 8,
         'disable_default_eval_metric': 1, # Flag to disable default metric. Set to >0 to disable.
         
         # Parameters for Tree Booster
         'eta': 0.01, # alias learning_rate
         'gamma': 1, # alias min_split_loss
         'max_depth': 2,
         'min_child_weight': 1,
         'max_delta_step': 2.941 ,
         'subsample': 0.2,
         'colsample_bytree': 0.9,
         'colsample_bylevel': 0.9,
         'colsample_bynode': 0.9,
         'lambda': 1,
         'alpha': 0,
         'tree_method': 'auto',
         'scale_pos_weight': 1,
         'updater': 'grow_colmaker,prune',
         'refresh_leaf': 1,
         'process_type': 'default',
         'grow_policy': 'depthwise',
         
         # Additional parameters for Dart Booster (booster=dart)
         'sample_type': 'uniform',
         'normalize_type': 'forest',
         'rate_drop': 0.1,
         'one_drop': 0,
         'skip_drop': 0.5,
         
         # Learning Task Parameters
         'objective': 'binary:logistic',
         'base_score': 0.5,
         # User can add multiple evaluation metrics. 
         # Python users: remember to pass the metrics in as list of 
         # parameters pairs instead of map, so that latter eval_metric won't override previous one
         'eval_metric':'aucpr', 
         'seed': 0
        } 
                
# Specify validations set to watch performance
evallist = [(dtest, 'eval'), (dtrain, 'train')]


# ## Objective and Evaluation Functions

# In[7]:


# baseado em https://github.com/dmlc/xgboost/blob/master/demo/guide-python/custom_objective.py
# user define objective function, given prediction, return gradient and second order gradient
# this is log likelihood loss
def logregobj(preds, dtrain):
    labels = dtrain.get_label()

#     preds = 1.0 / (1.0 + np.exp(-preds))
    
    grad = preds - labels
    hess = preds * (1.0 - preds)
    return grad, hess

def cupScoreObj(preds, dtrain):
    labels = dtrain.get_label()
    grad = 20*(labels**2) - 35*labels + 50*preds
    hess = 50 * (preds**0)
    return grad, hess

def cupScoreObj2(preds, dtrain):
    labels = dtrain.get_label()
    grad = 20*(labels**2) 
    hess = 50 * (preds**0)
    return grad, hess

def cupScoreObjJack(preds, dtrain):
#     S(T;P) = -35*T*P +5*T + 25*P
#     G(T;P) = -35*T+ 25
#     H(T;P) = -35
    labels = dtrain.get_label()
    grad = -35*labels*preds + 25
    hess = 0*(preds**0)
    return grad, hess

# Definição da função de avaliação para a DMC 2019
def cupEvalScore(y_predicted, y_true):
    labels = y_true.get_label()
    y_predicted = np.round(y_predicted)
    matrix = metrics.confusion_matrix(labels, y_predicted)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
    return 'cupScore', weightSum

def cupEvalScore_negative(y_predicted, y_true): # versão negativa da função
    labels = y_true.get_label()
    y_predicted = np.round(y_predicted)
    matrix = metrics.confusion_matrix(labels, y_predicted)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,25,5,-5]))
    return 'cupScore', weightSum

# Definição da função normalizada para avaliação para a DMC 2019
def cupEvalScoreNorm(y_predicted, y_true): # versão negativa da função
    labels = y_true.get_label()
    y_predicted = np.round(y_predicted)
    matrix = metrics.confusion_matrix(labels, y_predicted)
    totalArray = np.reshape(matrix, 4)
    weightSum = np.sum(totalArray*np.array([0,-25,-5,5]))/np.sum(totalArray)
    return 'cupScore', weightSum

def cupEvalScoreNorm_negative(y_predicted, y_true): # versão negativa da função
    labels = y_true.get_label()
    y_predicted = np.round(y_predicted)
    matrix = metrics.confusion_matrix(labels, y_predicted)
    totalArray = np.reshape(matrix, 4)
    weightSum = np.sum(totalArray*np.array([0,25,5,-5]))/np.sum(totalArray)
    return 'cupScore', weightSum

# user defined evaluation function, return a pair metric_name, result
# NOTE: when you do customized loss function, the default prediction value is margin
# this may make builtin evaluation metric not function properly
# for example, we are doing logistic loss, the prediction is score before logistic transformation
# the builtin evaluation error assumes input is after logistic transformation
# Take this in mind when you use the customization, and maybe you need write customized evaluation function
def evalerror(preds, dtrain):
    labels = dtrain.get_label()
    # return a pair metric_name, result. The metric name must not contain a colon (:) or a space
    # since preds are margin(before logistic transformation, cutoff at 0)
    return 'my-error', float(sum(labels != (preds > 0.0))) / len(labels)

# training with customized objective, we can also do step by step training
# simply look at xgboost.py's implementation of train
# bst = xgb.train(param, dtrain, num_round, watchlist, obj=logregobj, feval=evalerror)


# ## Training

# In[154]:


# Training a model requires a parameter list and data set.
num_round = 9000
# bst = xgb.train(param, dtrain, num_round, evallist, obj=cupScoreObjJack, feval=evalerror)
bst = xgb.train(params, dtrain, 
                num_boost_round=num_round,
                evals=evallist,
#                 obj=logregobj,
                feval=cupEvalScore,
                maximize=True,
                early_stopping_rounds=None,
                verbose_eval=100,
                xgb_model=None
               )

# # After training, the model can be saved.
# bst.save_model('0001.model')

# # The model and its feature map can also be dumped to a text file.
# # dump model
# bst.dump_model('dump.raw.txt')
# # dump model with feature map
# bst.dump_model('dump.raw.txt', 'featmap.txt')

# # A saved model can be loaded as follows:
# bst = xgb.Booster({'nthread': 4})  # init model
# bst.load_model('model.bin')  # load data


# ## Prediction

# In[69]:


# A model that has been trained or loaded can perform predictions on data sets.

# 7 entities, each contains 10 features
# data = np.random.rand(7, 10)
dtest = xgb.DMatrix(X_test)
ypred = bst.predict(dtest)


# In[71]:


results = list(map(int, ypred.round()))
# print(results)
matrix = confusion_matrix(y_test, results)

weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
print(f"Score Cup: {weightSum}")


# In[19]:


dtest = xgb.DMatrix(X)
ypred = bst.predict(dtest)


# In[201]:


results = list(map(int, ypred.round()))


# In[153]:


pd.DataFrame(np.zeros(1879)).count()


# In[143]:


np.reshape(matrix, 4)


# In[202]:


# y = list(map(int, ypred.round()))
matrix = confusion_matrix(Y, results)
# print(f"Score Cup: {0.055*}")
weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
print(f"Score Cup: {weightSum}")
matrix


# In[57]:


int(268.76260396042153)


# ## Optimization

# In[97]:


from __future__ import division

import pandas as pd
import numpy as np

import xgboost as xgb
from xgboost import XGBClassifier

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, RobustScaler
from sklearn import metrics
import sklearn.metrics as metrics
from sklearn.metrics import f1_score, roc_auc_score, roc_curve, auc

from bayes_opt import BayesianOptimization


from itertools import chain
import sys


# In[81]:


df = pd.read_csv('Datasets/train.csv', sep='|')
dall = xgb.DMatrix(df[df.columns[:-1].tolist()], label=pd.DataFrame(df[df.columns[-1]]))

train1 = pd.read_csv('Datasets/random_state_42/fold1/trn1.csv' , sep='|', index_col=0)
test1 = pd.read_csv('Datasets/random_state_42/fold1/tst1.csv', sep='|', index_col=0)

train2 = pd.read_csv('Datasets/random_state_42/fold2/trn2.csv', sep='|', index_col=0)
test2 = pd.read_csv('Datasets/random_state_42/fold2/tst2.csv', sep='|', index_col=0)

train3 = pd.read_csv('Datasets/random_state_42/fold3/trn3.csv', sep='|', index_col=0)
test3 = pd.read_csv('Datasets/random_state_42/fold3/tst3.csv', sep='|', index_col=0)


dtrain1 = xgb.DMatrix(train1[train1.columns[:-1].tolist()], label=pd.DataFrame(train1[train1.columns[-1]]))
dtest1 = xgb.DMatrix(test1[test1.columns[:-1].tolist()], label=pd.DataFrame(test1[test1.columns[-1]]))

dtrain2 = xgb.DMatrix(train2[train2.columns[:-1].tolist()], label=pd.DataFrame(train2[train2.columns[-1]]))
dtest2 = xgb.DMatrix(test2[test2.columns[:-1].tolist()], label=pd.DataFrame(test2[test2.columns[-1]]))

dtrain3 = xgb.DMatrix(train3[train3.columns[:-1].tolist()], label=pd.DataFrame(train3[train3.columns[-1]]))
dtest3 = xgb.DMatrix(test3[test3.columns[:-1].tolist()], label=pd.DataFrame(test3[test3.columns[-1]]))


# In[82]:


def cupEvalScore(y_predicted, y_true):
    labels = y_true
    y_predicted = np.round(y_predicted)
    matrix = metrics.confusion_matrix(labels, y_predicted)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
    return 'cupScore', weightSum


# In[132]:


def score_cup(y_tst , y_pred):
    matrix = metrics.confusion_matrix(y_tst, y_pred)
    return np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))

def best_threshold(y_tst, y_prob, incremento=0.1):
    threshold=incremento
    n_iter = np.linspace(0.0, 1.0, (1.0/incremento))
    scor = -9999999
    thr_start = 0.0
    thr_end= 0.0
    thr_set = False
#     print('11111')
    for n in n_iter:
#         print('2222')
        y_pred = (y_prob >= threshold).astype(int)
#         print('3333')
        sc = score_cup(y_tst, y_pred)
        if (sc > scor):
            scor = sc
            thr_start = threshold
            thr_end = threshold
            thr_set == True
        elif (sc == scor and (thr_set == True)):
            thr_end = threshold
        else:
            thr_set = False
        threshold+= incremento
    thr = (thr_start+thr_end)/2.0
    return thr, scor

def get_full_array(y1, y2, y3):
    fullarray = list(chain(y1,y2,y3))
    return np.array(fullarray)


# In[171]:


hehehe = {'booster': 'dart',
          # General Parameters
          'silent': 1,
          'verbosity': 0,
          'n_jobs': 7,
          'nthread': 7,
          'disable_default_eval_metric': 0, 
          
          # Parameters for Tree Booster
          'eta': 0.3058923588627213, # alias learning_rate
          'learning_rate': 0.3058923588627213,
          'gamma': 1.0039960961704106, # alias min_split_loss
          'min_split_loss': 0.6594434998494806,
          'max_depth': int(25.029595588074304),
          'min_child_weight': 1.047314340753419,
          'max_delta_step': 99.88690346965778,
          'subsample': 0.6168125387754934, 
          'colsample_bytree': 1.0,
          'colsample_bylevel': 1.0,
          'colsample_bynode': 0.8658913698078194,
          'lambda': 0.018030401719832174,
          'reg_lambda': 0.018030401719832174,
          'alpha': 0.0, 
          'reg_alpha': 0.0, 
          'tree_method': 'auto',
          'scale_pos_weight': 1,
          'updater': 'grow_colmaker,prune',
          'refresh_leaf': 1,
          'process_type': 'default',
          'grow_policy': 'depthwise',
          
          # Additional parameters for Dart Booster (booster=dart)
          'sample_type': 'uniform',
          'normalize_type': 'forest',
          'rate_drop':0.001,
          'one_drop': 0,
          'skip_drop': 1.0,
          
          # Learning Task Parameters
          'objective': 'binary:logistic',
          'base_score': 0.5,
          'eval_metric':'aucpr',
          'seed': 0,
          'random_state': 0,
          'n_estimators': 2000,
          'num_boost_round': 2000
         }

bst1 = xgb.train(hehehe, dtrain=dtrain1, evals=[(dtest1, 'val'), (dtrain1, 'trn')],
                     num_boost_round=hehehe['num_boost_round'], verbose_eval=2000)
bst2 = xgb.train(hehehe, dtrain=dtrain2, evals=[(dtest2, 'val'), (dtrain2, 'trn')],
                 num_boost_round=hehehe['num_boost_round'], verbose_eval=2000)
bst3 = xgb.train(hehehe, dtrain=dtrain3, evals=[(dtest3, 'val'), (dtrain3, 'trn')],
                 num_boost_round=hehehe['num_boost_round'], verbose_eval=2000)
preds1 = bst1.predict(dtest1)
y_true1 = test1[test1.columns[-1]]
best_thr1, best_score1 = best_threshold(y_true1, preds1, 0.001)

preds2 = bst2.predict(dtest2)
y_true2 = test2[test2.columns[-1]]
best_thr2, best_score2 = best_threshold(y_true2, preds2, 0.001)

preds3 = bst3.predict(dtest3)
y_true3 = test3[test3.columns[-1]]
best_thr3, best_score3 = best_threshold(y_true3, preds3, 0.001)


y_prob = get_full_array(preds1, preds2, preds3)
y_test = get_full_array(y_true1, y_true2, y_true3)
best_thr, best_score = best_threshold(y_test, y_prob, 0.001)

pred_labels1 = (preds1>=best_thr).astype(int)
aucpr1 = metrics.roc_auc_score(y_true1, pred_labels1)
f1_1 = metrics.f1_score(y_true1, pred_labels1)

pred_labels2 = (preds2>=best_thr).astype(int)
aucpr2 = metrics.roc_auc_score(y_true2, pred_labels2)
f1_2 = metrics.f1_score(y_true2, pred_labels2)

pred_labels3 = (preds3>=best_thr).astype(int)
aucpr3 = metrics.roc_auc_score(y_true3, pred_labels3)
f1_3 = metrics.f1_score(y_true3, pred_labels3)

print('Threshold: ', best_thr)
print('Scores: ', score_cup(y_true1, pred_labels1), 
      score_cup(y_true2, pred_labels2), score_cup(y_true3, pred_labels3))
# print('Scores: ', cupEvalScore(pred_labels1, y_true1), cupEvalScore(pred_labels2, y_true2), cupEvalScore(pred_labels3, y_true3))
print('Final score:', best_score)
print('AUC_PR: ', aucpr1, aucpr2, aucpr3)
print('F1 Score: ', f1_1, f1_2, f1_3)
print('Best thresholds: ', best_thr1, best_thr2, best_thr3)
print('Best scores: ', best_score1, best_score2, best_score3)

# [0]	val-aucpr:0.561594	trn-aucpr:0.791808
# [1999]	val-aucpr:0.942624	trn-aucpr:1
# [0]	val-aucpr:0.633337	trn-aucpr:0.870599
# [1999]	val-aucpr:0.905394	trn-aucpr:1
# [0]	val-aucpr:0.785763	trn-aucpr:0.797205
# [1999]	val-aucpr:0.992922	trn-aucpr:1
        
# [0]	val-aucpr:0.561594	trn-aucpr:0.791808
# [1999]	val-aucpr:0.956659	trn-aucpr:1
# [0]	val-aucpr:0.633337	trn-aucpr:0.870599
# [1999]	val-aucpr:0.870382	trn-aucpr:1
# [0]	val-aucpr:0.785763	trn-aucpr:0.797205
# [1999]	val-aucpr:0.990227	trn-aucpr:1


# In[183]:


def xgb_evaluate_f1(max_depth=20, gamma=0.2, eta=0.3, subsample=1.0, min_child_weight=1, max_delta_step=0, 
                    colsample_bytree=1, colsample_bylevel=1, colsample_bynode=1, num_boost_round=200,
                    lmbd = 1.0, alpha=0.0,
                    rate_drop=0.1, one_drop=0, skip_drop=0.5
                ):
    num_boost_round = int(num_boost_round)
    param = {'booster': 'dart', 
             # General Parameters
             'silent': 1,
             'verbosity': 0,
             'nthread': 7,
             'n_jobs': 7,
             'disable_default_eval_metric': 0, # Flag to disable default metric. Set to >0 to disable.
                
             # Parameters for Tree Booster
             'eta': eta, # alias learning_rate
             'learning_rate': eta, # alias learning_rate
             'gamma': gamma, # alias min_split_loss
             'min_split_loss': gamma, # alias min_split_loss
             'max_depth': int(max_depth),
             'min_child_weight': min_child_weight,
             'max_delta_step': max_delta_step,
             'subsample': subsample,
             'colsample_bytree': colsample_bytree,
             'colsample_bylevel': colsample_bylevel,
             'colsample_bynode': colsample_bynode,
             'lambda': lmbd,
             'reg_lambda': lmbd,
             'alpha': alpha,
             'reg_alpha': alpha, 
             'tree_method': 'auto',
             'scale_pos_weight': 1,
             'updater': 'grow_colmaker,prune',
             'refresh_leaf': 1,
             'process_type': 'default',
             'grow_policy': 'depthwise',

             # Additional parameters for Dart Booster (booster=dart)
             'sample_type': 'uniform',
             'normalize_type': 'forest',
             'rate_drop': rate_drop,
             'one_drop': 0,
             'skip_drop': skip_drop,

             # Learning Task Parameters
             'objective': 'binary:logistic',
             'base_score': 0.5,
             # User can add multiple evaluation metrics. 
             # Python users: remember to pass the metrics in as list of 
             # parameters pairs instead of map, so that latter eval_metric won't override previous one
             'eval_metric':'aucpr', 
             'seed': 0,
             'n_estimators': num_boost_round
            } 
#     num_boost_round = int(2000)
    bst1 = xgb.train(param, dtrain1, 
                     evals=[(dtest1, 'val'), (dtrain1, 'trn')], num_boost_round=num_boost_round,
                     verbose_eval=num_boost_round)
    bst2 = xgb.train(param, dtrain2, 
                     evals=[(dtest2, 'val'), (dtrain2, 'trn')], num_boost_round=num_boost_round,
                     verbose_eval=num_boost_round)
    bst3 = xgb.train(param, dtrain3, 
                     evals=[(dtest3, 'val'), (dtrain3, 'trn')], num_boost_round=num_boost_round,
                     verbose_eval=num_boost_round)
    
    preds1 = bst1.predict(dtest1)
    y_true1 = test1[test1.columns[-1]]
    best_thr1, best_score1 = best_threshold(y_true1, preds1, 0.001)
    
    preds2 = bst2.predict(dtest2)
    y_true2 = test2[test2.columns[-1]]
    best_thr2, best_score2 = best_threshold(y_true2, preds2, 0.001)
    
    preds3 = bst3.predict(dtest3)
    y_true3 = test3[test3.columns[-1]]
    best_thr3, best_score3 = best_threshold(y_true3, preds3, 0.001)

    
    y_prob = get_full_array(preds1, preds2, preds3)
    y_test = get_full_array(y_true1, y_true2, y_true3)
    best_thr, best_score = best_threshold(y_test, y_prob, 0.001)
    
    pred_labels1 = (preds1>=best_thr).astype(int)
    aucpr1 = metrics.roc_auc_score(y_true1, pred_labels1)
    f1_1 = metrics.f1_score(y_true1, pred_labels1)
        
    pred_labels2 = (preds2>=best_thr).astype(int)
    aucpr2 = metrics.roc_auc_score(y_true2, pred_labels2)
    f1_2 = metrics.f1_score(y_true2, pred_labels2)

    pred_labels3 = (preds3>=best_thr).astype(int)
    aucpr3 = metrics.roc_auc_score(y_true3, pred_labels3)
    f1_3 = metrics.f1_score(y_true3, pred_labels3)
    
    print('Threshold: ', best_thr)
    print('Scores: ', cupEvalScore(pred_labels1, y_true1), 
          cupEvalScore(pred_labels2, y_true2), cupEvalScore(pred_labels3, y_true3))
    print('Final score:', best_score)
    print('AUC_PR: ', aucpr1, aucpr2, aucpr3)
    print('F1 Score: ', f1_1, f1_2, f1_3)
    print('Best thresholds: ', best_thr1, best_thr2, best_thr3)
    print('Best scores: ', best_score1, best_score2, best_score3)
    return ((f1_1+f1_2+f1_3)/3.0)


# In[138]:


xgb_bo = BayesianOptimization(xgb_evaluate_f1, {'max_depth': (5, 50), 
                                             'gamma': (0.0, 2.0),
                                             'eta': (0.005, 0.5),
                                             'subsample': (0.1,0.5),
                                             'min_child_weight': (1, 20),
                                             'max_delta_step': (0, 100),
                                             'num_boost_round': (190,200),
                                             'colsample_bytree': (0.30, 1.0),
                                             'colsample_bylevel': (0.30, 1.0),
                                             'colsample_bynode': (0.30, 1.0),
                                             'lmbd': (0.00001, 1.0),
                                             'alpha': (0.00001, 1.0),
                                             'rate_drop': (0.00001, 1.0), 
#                                              'one_drop'=0, 
                                             'skip_drop': (0.00001, 1.0)
                                            }, random_state=42)


# Number of random explorations and iterations per maximization call of the optimizer

# In[186]:


init_pointss = 2
n_iters = 10


# Defining new bounds for parameters, if needed

# In[195]:


xgb_bo.set_bounds(new_bounds={"gamma": (0.00001, 0.35)})
xgb_bo.set_bounds(new_bounds={"eta": (0.005, 0.35)})
xgb_bo.set_bounds(new_bounds={"subsample": (0.0001, 0.65)})
xgb_bo.set_bounds(new_bounds={"num_boost_round": (50, 3000)})
xgb_bo.set_bounds(new_bounds={"colsample_bytree": (0.7, 1.0)})
xgb_bo.set_bounds(new_bounds={"colsample_bylevel": (0.7, 1.0)})
xgb_bo.set_bounds(new_bounds={"colsample_bynode": (0.7, 1.0)})
xgb_bo.set_bounds(new_bounds={"lmbd": (0.0, 1.0)})
xgb_bo.set_bounds(new_bounds={"alpha": (0.0, 1.0)})
xgb_bo.set_bounds(new_bounds={"rate_drop": (0.05, 0.2)})
xgb_bo.set_bounds(new_bounds={"skip_drop": (0.35, 0.65)})


# Points to explore, manually set

# In[155]:


# optuna found params
# xgb_bo.probe(
#     params={'max_depth': 8, 'gamma': 8.357269048407964e-05, 'eta': 0.2977459053463421, 'subsample': 0.783454584541423, 'min_child_weight': 1, 'max_delta_step': 0, 'num_boost_round': 500, 'colsample_bytree': 1.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'lmbd': 1.6501307620966078e-07, 'alpha': 1.166613432434418e-05, 'rate_drop': 0.21270606807102554, 
#         # 'one_drop'=0, 
#         'skip_drop': 0.18261384324631066
#     }, lazy=False,
# )

# Params found with optuna - F1 Score: 0,86516376451077948
# xgb_bo.probe(
#     params={ 'max_depth': 7, 'gamma': 0.05929591128314911, 'eta': 0.02029784466594782, 'subsample': 0.6069232352712038, 'min_child_weight': 2, 'max_delta_step': 0, 'num_boost_round': 2000, 'colsample_bytree': 1.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'lmbd': 1.0816181698286238e-07, 'alpha': 0.0114803100927989, 'rate_drop': 0.024737388313497213,    
#            # 'one_drop'=0, 
#         'skip_drop': 0.5279733554274589
#     }, lazy=False,
# )

# old_probes = [{'target': 0.6911163062536528, 'params': {'alpha': 0.5436225722878251, 'colsample_bylevel': 0.7455305032163971, 'colsample_bynode': 0.3989746488003463, 'colsample_bytree': 0.7308457326987552, 'eta': 0.181303913442525, 'gamma': 0.11972797185051132, 'lmbd': 0.3477451951284621, 'max_delta_step': 98.77827807050174, 'max_depth': 22.89226875775186, 'min_child_weight': 1.4927371842161787, 'num_boost_round': 192.71456022805918, 'rate_drop': 0.13694075303021966, 'skip_drop': 0.9939064443467085, 'subsample': 0.4165608309966097}}, {'target': 0.6939484126984127, 'params': {'alpha': 0.24106273223342037, 'colsample_bylevel': 0.3221516524716955, 'colsample_bynode': 0.3894516420807413, 'colsample_bytree': 0.7301759576768483, 'eta': 0.47941283822677033, 'gamma': 0.26247226912089405, 'lmbd': 0.3773304960160433, 'max_delta_step': 99.51905046728501, 'max_depth': 22.17996829032458, 'min_child_weight': 1.4421018750967103, 'num_boost_round': 192.49874482774715, 'rate_drop': 0.04531972384574673, 'skip_drop': 0.19459300975622432, 'subsample': 0.4219130945400619}}, {'target': 0.6941891149956608, 'params': {'alpha': 0.6665176947351452, 'colsample_bylevel': 0.3551978416737501, 'colsample_bynode': 0.9268663819838543, 'colsample_bytree': 0.9745824351424401, 'eta': 0.3020545609717988, 'gamma': 0.13466357110588745, 'lmbd': 0.09069422540300741, 'max_delta_step': 99.45008405354689, 'max_depth': 23.08098914829815, 'min_child_weight': 1.9938019872650623, 'num_boost_round': 193.94464303562577, 'rate_drop': 0.9553694323757497, 'skip_drop': 0.7402423287630624, 'subsample': 0.4025523193151297}}, {'target': 0.6943024541894598, 'params': {'alpha': 0.7259556788702394, 'colsample_bylevel': 0.9279771819668039, 'colsample_bynode': 0.920960496985582, 'colsample_bytree': 0.8459128821003365, 'eta': 0.32280566484637246, 'gamma': 0.16827992999009767, 'lmbd': 0.16162871409461377, 'max_delta_step': 89.85541885270793, 'max_depth': 32.289307684681546, 'min_child_weight': 1.1747439807159634, 'num_boost_round': 222.47089371713028, 'rate_drop': 0.6638382673389478, 'skip_drop': 0.006056522262372468, 'subsample': 0.22864644113399893}}, {'target': 0.7156051833471189, 'params': {'alpha': 0.31662011685063374, 'colsample_bylevel': 0.5494228180913037, 'colsample_bynode': 0.33892351260152703, 'colsample_bytree': 0.5506171108714955, 'eta': 0.19378878839790173, 'gamma': 0.09009228486759764, 'lmbd': 0.8392535684393928, 'max_delta_step': 99.91826030077793, 'max_depth': 23.762842276986568, 'min_child_weight': 1.0077710074624027, 'num_boost_round': 194.02674908522226, 'rate_drop': 0.19206552065716687, 'skip_drop': 0.3504074838502712, 'subsample': 0.35933700248165784}}, {'target': 0.7203065134099617, 'params': {'alpha': 0.8933315384495586, 'colsample_bylevel': 0.3757085244510457, 'colsample_bynode': 0.6632164146055588, 'colsample_bytree': 0.7166856514649349, 'eta': 0.48900575559374376, 'gamma': 0.6754976470852392, 'lmbd': 0.9352644993261957, 'max_delta_step': 97.76116140960893, 'max_depth': 22.405183672840423, 'min_child_weight': 1.672926448649994, 'num_boost_round': 194.92928322505978, 'rate_drop': 0.11204436963262027, 'skip_drop': 0.9180844649944494, 'subsample': 0.6087716392506105}}, {'target': 0.7474415204678362, 'params': {'alpha': 0.33230110676636915, 'colsample_bylevel': 0.5789873334885456, 'colsample_bynode': 0.8571905743295507, 'colsample_bytree': 0.7722026796292785, 'eta': 0.4523273142143759, 'gamma': 0.9482474861129335, 'lmbd': 0.613765658112076, 'max_delta_step': 99.69027058123778, 'max_depth': 24.32770191837443, 'min_child_weight': 1.1274398475381426, 'num_boost_round': 193.0257809295102, 'rate_drop': 0.1527447011452568, 'skip_drop': 0.46744939363325844, 'subsample': 0.7108098187094746}}, {'target': 0.7480336941426069, 'params': {'alpha': 0.6795750221742654, 'colsample_bylevel': 0.8652732207551346, 'colsample_bynode': 0.6728933623288788, 'colsample_bytree': 0.7850273141001723, 'eta': 0.4385209641067182, 'gamma': 0.2387891219743239, 'lmbd': 0.32084518042144194, 'max_delta_step': 99.37993224360223, 'max_depth': 25.349315543536918, 'min_child_weight': 3.6234959356064014, 'num_boost_round': 194.34867912737016, 'rate_drop': 0.19535425549251462, 'skip_drop': 0.3831610174338841, 'subsample': 0.6233163187048189}}, {'target': 0.8458616335771509, 'params': {'alpha': 0.0114803100927989, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.02029784466594782, 'gamma': 0.05929591128314911, 'lmbd': 1.0816181698286238e-07, 'max_delta_step': 0.0, 'max_depth': 7.0, 'min_child_weight': 1.0, 'num_boost_round': 2000.0, 'rate_drop': 0.024737388313497213, 'skip_drop': 0.5279733554274589, 'subsample': 0.6069232352712038}}, {'target': 0.8530698938537923, 'params': {'alpha': 1.166613432434418e-05, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.2977459053463421, 'gamma': 8.357269048407964e-05, 'lmbd': 1.6501307620966078e-07, 'max_delta_step': 0.0, 'max_depth': 8.0, 'min_child_weight': 1.0, 'num_boost_round': 500.0, 'rate_drop': 0.21270606807102554, 'skip_drop': 0.18261384324631066, 'subsample': 0.783454584541423}}]

# for result in old_probes:
#     xgb_bo.probe(params=result['params'], lazy=False)

second_batch = [{'target': 0.8597530431906937, 'params': {'alpha': 0.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 0.8955577296635292, 'colsample_bytree': 1.0, 'eta': 0.2892622430547672, 'gamma': 0.8396937972056397, 'lmbd': 0.06165874607567225, 'max_delta_step': 99.83842082199108, 'max_depth': 24.85631919982158, 'min_child_weight': 1.1108599742553882, 'num_boost_round': 194.21445399800916, 'rate_drop': 0.001, 'skip_drop': 1.0, 'subsample': 0.6818534717616441}}, {'target': 0.8613598673300166, 'params': {'alpha': 0.2968707020844896, 'colsample_bylevel': 0.8211956324658287, 'colsample_bynode': 0.7772560974557892, 'colsample_bytree': 0.9577973192051817, 'eta': 0.2600773746523064, 'gamma': 0.02192125565869052, 'lmbd': 0.0025788099163444947, 'max_delta_step': 60.22426829051923, 'max_depth': 14.300060374140742, 'min_child_weight': 1.3095489213896956, 'num_boost_round': 509.97747895522, 'rate_drop': 0.28503953493032574, 'skip_drop': 0.7133697685459865, 'subsample': 0.8608584506159322}}, {'target': 0.8620750191627723, 'params': {'alpha': 0.22081965010369903, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.5, 'gamma': 0.0, 'lmbd': 0.0, 'max_delta_step': 83.72085089219014, 'max_depth': 49.52700318584637, 'min_child_weight': 1.0, 'num_boost_round': 510.0, 'rate_drop': 0.745577847030424, 'skip_drop': 1.0, 'subsample': 0.9}}, {'target': 0.862366737739872, 'params': {'alpha': 1.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.5, 'gamma': 0.3314786322574126, 'lmbd': 0.0, 'max_delta_step': 1.378424307693674, 'max_depth': 20.194244280221895, 'min_child_weight': 1.0, 'num_boost_round': 369.791192858813, 'rate_drop': 1.0, 'skip_drop': 1.0, 'subsample': 0.9}}, {'target': 0.8623693128560098, 'params': {'alpha': 0.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.5, 'gamma': 0.0, 'lmbd': 0.0, 'max_delta_step': 82.21517686887569, 'max_depth': 50.0, 'min_child_weight': 3.169212094913604, 'num_boost_round': 510.0, 'rate_drop': 1.0, 'skip_drop': 1.0, 'subsample': 0.9}}, {'target': 0.8626600652909785, 'params': {'alpha': 0.661031431359423, 'colsample_bylevel': 0.8940462916386449, 'colsample_bynode': 0.9144893424300642, 'colsample_bytree': 0.5750933915878902, 'eta': 0.25570713326978317, 'gamma': 0.58086991578021, 'lmbd': 0.17783727393698423, 'max_delta_step': 99.07851608847254, 'max_depth': 44.47239157373778, 'min_child_weight': 2.1818428669276155, 'num_boost_round': 422.96549694974317, 'rate_drop': 0.35312574458522505, 'skip_drop': 0.7867193541016351, 'subsample': 0.8381642041104226}}, {'target': 0.8635500093127212, 'params': {'alpha': 0.0, 'colsample_bylevel': 0.9865649807923106, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.5, 'gamma': 0.5338216424969991, 'lmbd': 0.0, 'max_delta_step': 80.3964597577857, 'max_depth': 48.47436705164977, 'min_child_weight': 4.229024744362612, 'num_boost_round': 510.0, 'rate_drop': 0.9941342719534593, 'skip_drop': 1.0, 'subsample': 0.9}}, {'target': 0.865043751256754, 'params': {'alpha': 0.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.5, 'gamma': 0.0, 'lmbd': 0.0, 'max_delta_step': 62.651318442069055, 'max_depth': 15.9482349294255, 'min_child_weight': 1.2857280190752516, 'num_boost_round': 509.18547260034325, 'rate_drop': 0.001, 'skip_drop': 1.0, 'subsample': 0.9}}, {'target': 0.865967365967366, 'params': {'alpha': 0.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.5, 'gamma': 0.0, 'lmbd': 0.0, 'max_delta_step': 3.071555690760593, 'max_depth': 9.29003033128633, 'min_child_weight': 1.0, 'num_boost_round': 502.871644053178, 'rate_drop': 0.001, 'skip_drop': 0.001, 'subsample': 0.9}}, {'target': 0.8667774051995364, 'params': {'alpha': 0.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 0.5802434441577181, 'colsample_bytree': 1.0, 'eta': 0.5, 'gamma': 0.0, 'lmbd': 0.0, 'max_delta_step': 69.4308275564914, 'max_depth': 10.002287525620247, 'min_child_weight': 1.0, 'num_boost_round': 506.1567414486162, 'rate_drop': 0.001, 'skip_drop': 1.0, 'subsample': 0.9}}, {'target': 0.8704501209368178, 'params': {'alpha': 0.6949305174698855, 'colsample_bylevel': 0.8237991003645367, 'colsample_bynode': 0.8474562965447179, 'colsample_bytree': 1.0, 'eta': 0.35999294074073596, 'gamma': 0.1140388530670303, 'lmbd': 0.0008725716951620838, 'max_delta_step': 58.17052520641333, 'max_depth': 5.658370792417768, 'min_child_weight': 1.8229035762804973, 'num_boost_round': 355.9122974142183, 'rate_drop': 0.9141271975350812, 'skip_drop': 0.9778402308737374, 'subsample': 0.5963188854400893}}, {'target': 0.8708836768538261, 'params': {'alpha': 0.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.5, 'gamma': 0.0, 'lmbd': 0.0, 'max_delta_step': 61.25678866870297, 'max_depth': 16.532726192015307, 'min_child_weight': 1.0, 'num_boost_round': 510.0, 'rate_drop': 0.001, 'skip_drop': 1.0, 'subsample': 0.9}}, {'target': 0.8708836768538261, 'params': {'alpha': 0.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.5, 'gamma': 0.0, 'lmbd': 0.0, 'max_delta_step': 80.42150799377411, 'max_depth': 50.0, 'min_child_weight': 1.0, 'num_boost_round': 510.0, 'rate_drop': 1.0, 'skip_drop': 1.0, 'subsample': 0.9}}, {'target': 0.8708836768538261, 'params': {'alpha': 0.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.5, 'gamma': 0.0, 'lmbd': 0.0, 'max_delta_step': 2.566316269104598, 'max_depth': 38.860948218119596, 'min_child_weight': 1.0, 'num_boost_round': 510.0, 'rate_drop': 0.001, 'skip_drop': 1.0, 'subsample': 0.9}}, {'target': 0.8708836768538261, 'params': {'alpha': 0.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.5, 'gamma': 0.0, 'lmbd': 0.0, 'max_delta_step': 4.728619598259209, 'max_depth': 45.1693371630415, 'min_child_weight': 1.0, 'num_boost_round': 510.0, 'rate_drop': 0.001, 'skip_drop': 1.0, 'subsample': 0.9}}, {'target': 0.8708836768538261, 'params': {'alpha': 0.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.5, 'gamma': 0.0, 'lmbd': 0.0, 'max_delta_step': 0.0, 'max_depth': 49.47143677608009, 'min_child_weight': 1.0, 'num_boost_round': 510.0, 'rate_drop': 0.001, 'skip_drop': 1.0, 'subsample': 0.9}}, {'target': 0.8717171717171718, 'params': {'alpha': 0.045655061238297275, 'colsample_bylevel': 0.9888474045382842, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.44998212978085533, 'gamma': 0.0, 'lmbd': 0.0, 'max_delta_step': 61.48080578321172, 'max_depth': 15.009562351768126, 'min_child_weight': 1.0546310720721699, 'num_boost_round': 509.80141297198116, 'rate_drop': 0.001, 'skip_drop': 1.0, 'subsample': 0.9}}, {'target': 0.8719736313447717, 'params': {'alpha': 0.5641013078922922, 'colsample_bylevel': 0.7934085379894419, 'colsample_bynode': 0.950390769693725, 'colsample_bytree': 1.0, 'eta': 0.3468065792032562, 'gamma': 0.850278695108028, 'lmbd': 0.0, 'max_delta_step': 75.66486025244092, 'max_depth': 50.0, 'min_child_weight': 1.4570051765271386, 'num_boost_round': 491.1254648537271, 'rate_drop': 0.7805442331513087, 'skip_drop': 0.6814656189534384, 'subsample': 0.8170615924999037}}, {'target': 0.873173092845224, 'params': {'alpha': 0.04715149675873525, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.35061315095326356, 'gamma': 1.0431558154581217, 'lmbd': 0.0, 'max_delta_step': 98.3432499132189, 'max_depth': 44.977390826386426, 'min_child_weight': 2.034178624229822, 'num_boost_round': 423.3927988113729, 'rate_drop': 1.0, 'skip_drop': 0.6245429965365635, 'subsample': 0.9}}, {'target': 0.8749670164126332, 'params': {'alpha': 0.0, 'colsample_bylevel': 0.991526865690946, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.5, 'gamma': 0.0, 'lmbd': 0.0, 'max_delta_step': 82.37542846139965, 'max_depth': 50.0, 'min_child_weight': 1.4379439602936295, 'num_boost_round': 510.0, 'rate_drop': 0.8885171058212279, 'skip_drop': 1.0, 'subsample': 0.9}}, {'target': 0.8751241455183177, 'params': {'alpha': 0.09714759100654702, 'colsample_bylevel': 1.0, 'colsample_bynode': 0.8304847794362, 'colsample_bytree': 1.0, 'eta': 0.24020327094176086, 'gamma': 0.6302496312976126, 'lmbd': 0.0, 'max_delta_step': 100.0, 'max_depth': 24.892526262892783, 'min_child_weight': 1.1833787779058804, 'num_boost_round': 194.5893425738647, 'rate_drop': 0.001, 'skip_drop': 1.0, 'subsample': 0.42271859934382683}}, {'target': 0.8754247017844285, 'params': {'alpha': 0.07847189839415822, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.5, 'gamma': 0.0, 'lmbd': 0.0, 'max_delta_step': 0.0, 'max_depth': 40.0905677833493, 'min_child_weight': 1.0, 'num_boost_round': 510.0, 'rate_drop': 0.001, 'skip_drop': 1.0, 'subsample': 0.9}}, {'target': 0.8766368902453188, 'params': {'alpha': 0.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 0.8195306931589529, 'colsample_bytree': 1.0, 'eta': 0.3435497097535312, 'gamma': 0.8253330354358924, 'lmbd': 0.0, 'max_delta_step': 99.93614467791717, 'max_depth': 25.279821548158992, 'min_child_weight': 1.0, 'num_boost_round': 194.83837758463093, 'rate_drop': 0.001, 'skip_drop': 1.0, 'subsample': 0.5522890679096606}}, {'target': 0.8770145739455203, 'params': {'alpha': 0.07054604615835453, 'colsample_bylevel': 0.9973837801211652, 'colsample_bynode': 0.9980058962483493, 'colsample_bytree': 0.9934404423542881, 'eta': 0.49448661111130005, 'gamma': 0.0052487969401762, 'lmbd': 0.0025959203234875426, 'max_delta_step': 80.87503036026946, 'max_depth': 49.53166136225028, 'min_child_weight': 1.0035580544454605, 'num_boost_round': 509.9948828471993, 'rate_drop': 0.911228493952634, 'skip_drop': 0.9899139886061744, 'subsample': 0.8931579711525269}}, {'target': 0.8792105048527853, 'params': {'alpha': 0.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 0.8007099781339093, 'colsample_bytree': 1.0, 'eta': 0.2523305995780385, 'gamma': 1.3497088702041546, 'lmbd': 0.0, 'max_delta_step': 99.92640287114386, 'max_depth': 25.131660727123652, 'min_child_weight': 1.0, 'num_boost_round': 194.842439292914, 'rate_drop': 0.001, 'skip_drop': 1.0, 'subsample': 0.4758736895761753}}, {'target': 0.8808137475739158, 'params': {'alpha': 0.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 0.9216368392548205, 'colsample_bytree': 1.0, 'eta': 0.5, 'gamma': 0.350840641152095, 'lmbd': 0.04928942730730019, 'max_delta_step': 64.56369819950532, 'max_depth': 18.95248953226242, 'min_child_weight': 3.9736557448578917, 'num_boost_round': 510.0, 'rate_drop': 0.001, 'skip_drop': 1.0, 'subsample': 0.9}}, {'target': 0.8872920380273323, 'params': {'alpha': 0.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.5, 'gamma': 0.0, 'lmbd': 0.0, 'max_delta_step': 2.3049401949676103, 'max_depth': 10.214892390101175, 'min_child_weight': 1.0, 'num_boost_round': 503.06964664328893, 'rate_drop': 0.001, 'skip_drop': 0.001, 'subsample': 0.9}}, {'target': 0.8938781975132652, 'params': {'alpha': 0.0114803100927989, 'colsample_bylevel': 1.0, 'colsample_bynode': 1.0, 'colsample_bytree': 1.0, 'eta': 0.02029784466594782, 'gamma': 0.05929591128314911, 'lmbd': 1.0816181698286238e-07, 'max_delta_step': 0.0, 'max_depth': 7.0, 'min_child_weight': 1.0, 'num_boost_round': 2000.0, 'rate_drop': 0.024737388313497213, 'skip_drop': 0.5279733554274589, 'subsample': 0.6069232352712038}}, {'target': 0.9018794914317304, 'params': {'alpha': 0.0, 'colsample_bylevel': 1.0, 'colsample_bynode': 0.8658913698078194, 'colsample_bytree': 1.0, 'eta': 0.3058923588627213, 'gamma': 1.0039960961704106, 'lmbd': 0.018030401719832174, 'max_delta_step': 99.88690346965778, 'max_depth': 25.029595588074304, 'min_child_weight': 1.047314340753419, 'num_boost_round': 194.4636142754498, 'rate_drop': 0.001, 'skip_drop': 1.0, 'subsample': 0.6168125387754934}}, {'target': 0.9035663744131878, 'params': {'alpha': 0.48538480042807625, 'colsample_bylevel': 1.0, 'colsample_bynode': 0.6199685796603064, 'colsample_bytree': 0.978246793827651, 'eta': 0.4195320192238707, 'gamma': 0.6594434998494806, 'lmbd': 0.011773682500974159, 'max_delta_step': 97.95851463554885, 'max_depth': 45.19892892494292, 'min_child_weight': 1.6016252598219185, 'num_boost_round': 423.23652314159165, 'rate_drop': 1.0, 'skip_drop': 0.8093763776031284, 'subsample': 0.9}}]
new_b = second_batch.copy()[-10:]
for result in new_b:
    new_p = result['params']
    new_p['num_boost_round'] = 2000
    xgb_bo.probe(params=new_p, lazy=False)


# In[196]:


# Maximizing using different acquisition functions - https://github.com/fmfn/BayesianOptimization/blob/master/examples/exploitation_vs_exploration.ipynb

# Acquisition Function "Probability of Improvement"
# Prefer exploration (xi=0.1)
xgb_bo.maximize(init_points=init_pointss, n_iter=n_iters, acq='poi', xi=1e-1)
# Prefer exploitation (xi=0.0)
xgb_bo.maximize(init_points=init_pointss, n_iter=n_iters, acq='poi', xi=1e-4)

# Acquisition Function "Expected Improvement"
# Prefer exploration (xi=0.1)
xgb_bo.maximize(init_points=init_pointss, n_iter=n_iters, acq='ei', xi=1e-1)
# Prefer exploitation (xi=0.0)
xgb_bo.maximize(init_points=init_pointss, n_iter=n_iters, acq='ei', xi=1e-4)

# Acquisition Function "Upper Confidence Bound"
# Prefer exploitation (kappa=1.0)
xgb_bo.maximize(init_points=init_pointss, n_iter=n_iters, acq="ucb", kappa=1e-1)
# Prefer exploration (kappa=10)
xgb_bo.maximize(init_points=init_pointss, n_iter=n_iters, acq="ucb", kappa=10)
# |   iter    |  target   |   alpha   | colsam... | colsam... | colsam... |    eta  | gamma | lmbd | max_de... 
# | max_depth | min_ch... | num_bo... | rate_drop | skip_drop | subsample |


# In[200]:


# |   iter    |  target   |   alpha   | colsam... | colsam... | colsam... |    eta  | gamma | lmbd | max_de... 
# | max_depth | min_ch... | num_bo... | rate_drop | skip_drop | subsample |
params = xgb_bo.max['params']
# print(xgb_bo.res[-4:])
newlist = sorted(xgb_bo.res, key=lambda k: k['target']) 
print(newlist[-1:])

for item in newlist[-19:]:
    print(item['params']['rate_drop'],item['params']['skip_drop'])


# In[118]:





# In[113]:


xgb_bo.maximize(init_points=init_pointss, n_iter=n_iters, acq='poi', xi=1e-1)


# In[114]:


xgb_bo.maximize(init_points=init_pointss, n_iter=n_iters, acq='poi', xi=1e-4)


# In[122]:





# In[66]:


params = xgb_bo.max['params']
print(params)
newlist = sorted(xgb_bo.res, key=lambda k: k['target']) 
print(newlist[-10:])


# In[117]:


# {'max_delta_step': 0.18382135352862372,
#  'max_depth': 28.78527992386557,
#  'min_child_weight': 1.0802329841538665,
#  'subsample': 0.2449319469967698}


# {'max_delta_step': 1.2521177666231842,
#  'max_depth': 11.004852045755626,
#  'min_child_weight': 1.3911472293168208,
#  'subsample': 0.21680063506771674}


# Best dart found so far
# 'max_depth':20, 
# 'gamma':0.2, 
# 'eta': 0.374510074002978, 
# 'subsample': 0.3901119166914734, 
# 'num_boost_round': 268,
# 'min_child_weight':1, 
# 'max_delta_step':0,
# 'colsample_bytree':1, 
# 'colsample_bylevel':1, 
# 'colsample_bynode':1,  

def xgb_evaluate(max_depth=20, gamma=0.2, eta=0.3, subsample=1.0, min_child_weight=1, max_delta_step=0, 
                 colsample_bytree=1, colsample_bylevel=1, colsample_bynode=1, num_boost_round=500      
                ):
    params = {'booster': 'dart', # General Parameters
         'silent': 1,
         'verbosity': 1,
         'nthread': 7,
         'disable_default_eval_metric': 0, # Flag to disable default metric. Set to >0 to disable.
         
         # Parameters for Tree Booster
         'eta': eta, # alias learning_rate
         'gamma': gamma, # alias min_split_loss
         'max_depth': int(max_depth),
         'min_child_weight': min_child_weight,
         'max_delta_step': max_delta_step,
         'subsample': subsample,
         'colsample_bytree': colsample_bytree,
         'colsample_bylevel': colsample_bylevel,
         'colsample_bynode': colsample_bynode,
         'lambda': 1,
         'alpha': 0,
         'tree_method': 'auto',
         'scale_pos_weight': 1,
         'updater': 'grow_colmaker,prune',
         'refresh_leaf': 1,
         'process_type': 'default',
         'grow_policy': 'depthwise',
         
         # Additional parameters for Dart Booster (booster=dart)
         'sample_type': 'uniform',
         'normalize_type': 'forest',
         'rate_drop': 0.1,
         'one_drop': 0,
         'skip_drop': 0.5,
         
         # Learning Task Parameters
         'objective': 'binary:logistic',
         'base_score': 0.5,
         # User can add multiple evaluation metrics. 
         # Python users: remember to pass the metrics in as list of 
         # parameters pairs instead of map, so that latter eval_metric won't override previous one
         'eval_metric':'aucpr', 
         'seed': 0
        } 
    
    # Used around 1000 boosting rounds in the full model
    cv_result = xgb.cv(params, dall,
                       num_boost_round=int(num_boost_round),
                       nfold=3,
                       stratified=True,
#                        evals=evallist,
#                        obj=logregobj,
                       feval=cupEvalScore,
                       maximize=True,
                       early_stopping_rounds=None,
                       verbose_eval=False,
                       seed=0
                      )    
    
    # Bayesian optimization only knows how to maximize, not minimize, so return the negative RMSE
#     print('Score: ', cv_result['test-cupScore-mean'].iloc[-1])
    print('AUC_PR: ', cv_result['test-aucpr-mean'].iloc[-1], ' ', cv_result['train-aucpr-mean'].iloc[-1])
    return cv_result['test-aucpr-mean'].iloc[-1]
#     return cv_result['test-cupScore-mean'].iloc[-1]


# In[70]:


# # cv_result = xgb.cv(param, dtrain, num_boost_round=1000, nfold=3)
# cv_result = xgb.cv(params, dall,
# #                    num_boost_round=500,
#                    nfold=3,
#                    stratified=True,
# #                        evals=evallist,
# #                        obj=logregobj,
#                    feval=cupEvalScore,
#                    maximize=False,
#                    early_stopping_rounds=None,
#                    verbose_eval=False,
#                    seed=0
#                   )  
# # cv_result

from bayes_opt.observer import JSONLogger
from bayes_opt.event import Events

logger = JSONLogger(path="./logs.json")
xgb_bo.subscribe(Events.OPTMIZATION_STEP, logger)


# In[118]:


# https://github.com/fmfn/BayesianOptimization/wiki/Introduction
xgb_bo = BayesianOptimization(xgb_evaluate, {
#     'max_depth': (10, 50), 
#                                              'gamma': (0.0, 2.0),
                                             'eta': (0.005, 0.5),
                                             'subsample': (0.1,0.5),
#                                              'min_child_weight': (1, 20),
#                                              'max_delta_step': (0, 100),
                                             'num_boost_round': (50,200)
#                                              'colsample_bytree': (0.30, 1.0),
#                                              'colsample_bylevel': (0.30, 1.0),
#                                              'colsample_bynode': (0.30, 1.0),
                                            }, random_state=42)

xgb_bo.probe(
    params={
#         'max_depth': 20, 
#              'gamma': 0.2,
       'eta': 0.1,
             'subsample': 0.2,
             'num_boost_round': 100,
#              'min_child_weight': 2,
#              'max_delta_step': 1
    },
    lazy=True,
)

xgb_bo.probe(
    params={
#         'max_depth': 20, 
#              'gamma': 0.2,
               'eta': 0.3,
             'subsample': 0.2,
             'num_boost_round': 200,
#              'min_child_weight': 2,
#              'max_delta_step': 1
    },
    lazy=True,
)

xgb_bo.probe(
    params={
#         'max_depth': 20, 
#              'gamma': 0.2,
               'eta': 0.5,
             'subsample': 0.2,
             'num_boost_round': 200,
#              'min_child_weight': 2,
#              'max_delta_step': 1
    },
    lazy=True,
)

xgb_bo.probe(
    params={
#         'max_depth': 20, 
#              'gamma': 0.2,
               'eta': 0.45,
             'subsample': 0.35,
             'num_boost_round': 120,
#              'min_child_weight': 2,
#              'max_delta_step': 1
    },
    lazy=True,
)

xgb_bo.probe(
    params={
#         'max_depth': 20, 
#              'gamma': 0.2,
               'eta': 0.4,
             'subsample': 0.35,
             'num_boost_round': 200,
#              'min_child_weight': 2,
#              'max_delta_step': 1
    },
    lazy=True,
)


xgb_bo.probe(
    params={
#         'max_depth': 20, 
#              'gamma': 0.2,
               'eta': 0.035,
             'subsample': 0.2,
             'num_boost_round': 200,
#              'min_child_weight': 2,
#              'max_delta_step': 1
    },
    lazy=True,
)

xgb_bo.probe(
    params={
#         'max_depth': 20, 
#              'gamma': 0.2,
               'eta': 0.035,
             'subsample': 0.2,
             'num_boost_round': 200,
#              'min_child_weight': 2,
#              'max_delta_step': 1
    },
    lazy=True,
)

xgb_bo.probe(
    params={
#         'max_depth': 20, 
#              'gamma': 0.2,
               'eta': 0.33,
             'subsample': 0.36,
             'num_boost_round': 100,
#              'min_child_weight': 2,
#              'max_delta_step': 1
    },
    lazy=True,
)


# In[96]:


xgb_bo.set_bounds(new_bounds={"num_boost_round": (200, 300)})
xgb_bo.set_bounds(new_bounds={"subsample": (0.15, 0.35)})
xgb_bo.set_bounds(new_bounds={"eta": (0.1, 0.4)})

init_pointss = 2
n_iters = 10


# In[119]:


xgb_bo.maximize(init_points=init_pointss, n_iter=n_iters, acq='poi', xi=1e-1)


# In[120]:


xgb_bo.maximize(init_points=init_pointss, n_iter=n_iters, acq='ei', xi=1e-4)


# In[97]:


xgb_bo.maximize(init_points=init_pointss, n_iter=n_iters, acq='ei', xi=1e-1)


# In[99]:


xgb_bo.maximize(init_points=init_pointss, n_iter=n_iters, acq='poi', xi=1e-4)


# In[100]:


xgb_bo.maximize(init_points=init_pointss, n_iter=n_iters, acq="ucb", kappa=10)


# In[101]:


params = xgb_bo.max['params']
params


# In[92]:


print(xgb_bo.res)


# In[103]:


print(xgb_bo.res)


# In[110]:


newlist = sorted(xgb_bo.res, key=lambda k: k['target']) 
print(newlist[-10:-1])


# Top 10 best results found for DART booster AUC_PR of test set as target:
# 
# (there are actually only 9 results because one was duplicated)

# In[116]:


best_dart = [{'target': 0.9072546666666667, 
  'params': {'eta': 0.46223494459233166, 'num_boost_round': 249, 'subsample': 0.5}}, 
 {'target': 0.908811, 
  'params': {'eta': 0.15763138515098732, 'num_boost_round': 730, 'subsample': 0.38472861117518387}}, 
 {'target': 0.9099266666666667, 
  'params': {'eta': 0.1674104263181474, 'num_boost_round': 134, 'subsample': 0.46319416536789104}}, 
 {'target': 0.9102796666666668, 
  'params': {'eta': 0.42449710984691236, 'num_boost_round': 193, 'subsample': 0.5}}, 
 {'target': 0.9121146666666666, 
  'params': {'eta': 0.14335843863615289, 'num_boost_round': 134, 'subsample': 0.5}}, 
 {'target': 0.9126083333333334, 
  'params': {'eta': 0.14912839736856998, 'num_boost_round': 730, 'subsample': 0.39395166898239}}, 
 {'target': 0.9162983333333333, 
  'params': {'eta': 0.3690610937345273, 'num_boost_round': 281, 'subsample': 0.34999587130860893}}, 
 {'target': 0.9167740000000001, 
  'params': {'eta': 0.4, 'num_boost_round': 281, 'subsample': 0.35}},
 {'target': 0.9240173333333334, 
  'params': {'eta': 0.38299118824273026, 'num_boost_round': 281, 'subsample': 0.35}}]

best_params = best_dart[-1]['params']
eta = best_params['eta']
num_boost_round = best_params['num_boost_round']
subsample = best_params['num_boost_round']

params = {'booster': 'dart', # General Parameters
     'silent': 1,
     'verbosity': 1,
     'nthread': 7,
     'disable_default_eval_metric': 0, # Flag to disable default metric. Set to >0 to disable.

     # Parameters for Tree Booster
     'eta': eta, # alias learning_rate
     'gamma': 0.2, # alias min_split_loss
     'max_depth': int(20),
     'min_child_weight': 1,
     'max_delta_step': 0,
     'subsample': subsample,
     'colsample_bytree': 1.0,
     'colsample_bylevel': 1.0,
     'colsample_bynode': 1.0,
     'lambda': 1,
     'alpha': 0,
     'tree_method': 'auto',
     'scale_pos_weight': 1,
     'updater': 'grow_colmaker,prune',
     'refresh_leaf': 1,
     'process_type': 'default',
     'grow_policy': 'depthwise',

     # Additional parameters for Dart Booster (booster=dart)
     'sample_type': 'uniform',
     'normalize_type': 'forest',
     'rate_drop': 0.1,
     'one_drop': 0,
     'skip_drop': 0.5,

     # Learning Task Parameters
     'objective': 'binary:logistic',
     'base_score': 0.5,
     # User can add multiple evaluation metrics. 
     # Python users: remember to pass the metrics in as list of 
     # parameters pairs instead of map, so that latter eval_metric won't override previous one
     'eval_metric':'aucpr', 
     'seed': 0
    } 

params


# In[113]:


params = xgb_bo.max['params']
params

xgb_bo.max


# In[48]:



# Use the expected improvement acquisition function to handle negative numbers
# Optimally needs quite a few more initiation points and number of iterations
xgb_bo.maximize(init_points=4, n_iter=20, acq='ei')

# |   iter    |  target   | colsam... | colsam... | colsam... |    eta    |   gamma   | max_de... | max_depth | min_ch... | subsample |
# -------------------------------------------------------------------------------------------------------------------------------------
# |  24       | -6.667    |  0.55     |  0.7522   |  0.6686   |  0.05374  |  0.5575   |  0.8362   |  13.1     |  2.099    |  0.6404   |
# |  42       |  3.333    |  0.8535   |  0.8694   |  0.6251   |  0.06864  |  0.4289   |  0.8851   |  13.34    |  2.426    |  0.7941   |
# |  49       |  10.0     |  0.8943   |  0.8539   |  0.75     |  0.07257  |  0.7159   |  0.6511   |  13.87    |  2.343    |  0.7006   |


# In[37]:


xgb_bo.probe(
    params={
    'eta': 0.33171396534540043,
 'num_boost_round': 118.56847151243234,
 'subsample': 0.35918515803252393},
    lazy=True
)


# In[49]:


xgb_bo.maximize(init_points=4, n_iter=50, acq='poi', xi=1e-1)


# In[50]:


xgb_bo.maximize(init_points=4, n_iter=50, acq="ei", xi=1e-4)


# In[31]:


# https://github.com/fmfn/BayesianOptimization/wiki/Introduction
xgb_bo = BayesianOptimization(xgb_evaluate, {'max_depth': (2, 50), 
                                             'gamma': (0.0, 1.0),
                                             'eta': (0.001, 0.1),
                                             'subsample': (0.05,0.4),
                                             'min_child_weight': (1, 20),
                                             'max_delta_step': (0, 20),
#                                              'colsample_bytree': (0.30, 0.90),
#                                              'colsample_bylevel': (0.30, 0.90),
#                                              'colsample_bynode': (0.30, 0.90),
                                            })
# Use the expected improvement acquisition function to handle negative numbers
# Optimally needs quite a few more initiation points and number of iterations
xgb_bo.maximize(init_points=4, n_iter=100, acq='ei')

# |   iter    |  target   | colsam... | colsam... | colsam... |    eta    |   gamma   | max_de... | max_depth | min_ch... | subsample |
# -------------------------------------------------------------------------------------------------------------------------------------
# |  24       | -6.667    |  0.55     |  0.7522   |  0.6686   |  0.05374  |  0.5575   |  0.8362   |  13.1     |  2.099    |  0.6404   |
# |  42       |  3.333    |  0.8535   |  0.8694   |  0.6251   |  0.06864  |  0.4289   |  0.8851   |  13.34    |  2.426    |  0.7941   |
# |  49       |  10.0     |  0.8943   |  0.8539   |  0.75     |  0.07257  |  0.7159   |  0.6511   |  13.87    |  2.343    |  0.7006   |


# In[159]:


# https://github.com/fmfn/BayesianOptimization/wiki/Introduction
xgb_bo = BayesianOptimization(xgb_evaluate, {'max_depth': (2, 50), 
                                             'gamma': (0.0, 1.0),
                                             'colsample_bytree': (0.30, 0.90),
                                             'eta': (0.001, 0.1),
                                             'subsample': (0.05,0.90),
                                             'colsample_bylevel': (0.30, 0.90),
                                             'colsample_bynode': (0.30, 0.90),
                                             'min_child_weight': (1, 20),
                                             'max_delta_step': (0, 20),
                                            })
# Use the expected improvement acquisition function to handle negative numbers
# Optimally needs quite a few more initiation points and number of iterations
xgb_bo.maximize(init_points=4, n_iter=30, acq='ei')


# ## Extract the parameters of the best model.
# 
# params = xgb_bo.res['max']['max_params']
# 
# params['max_depth'] = int(params['max_depth'])

# In[38]:


params = xgb_bo.max['params']
# params['max_depth'] = int(params['max_depth'])

params


# In[56]:


params = xgb_bo.max['params']
# params['max_depth'] = int(params['max_depth'])

params


# In[64]:


xgb_bo.res[477]


# In[ ]:


AUC_PR:  0.9230516666666665   0.999844
|  478      |  3.333    |  0.3912   |  268.8    |  0.3844   |


# 4. Saving, loading and restarting¶ 

# In[34]:


from bayes_opt.observer import JSONLogger
from bayes_opt.event import Events

logger = JSONLogger(path="./logs.json")
xgb_bo.subscribe(Events.OPTMIZATION_STEP, logger)


# ### Rascunho

# In[ ]:


import pandas as pd
import numpy as np
import os

from sklearn.preprocessing import StandardScaler, RobustScaler
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.metrics import f1_score, roc_auc_score
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.neural_network import MLPClassifier

from imblearn.pipeline import make_pipeline as make_pipeline_imb
from imblearn.over_sampling import SMOTE
from imblearn.metrics import classification_report_imbalanced

import xgboost as xgb
from xgboost import XGBClassifier

import matplotlib.pyplot as plt
from collections import Counter

import time
import warnings
# warnings.filterwarnings('ignore')


# In[88]:


# Baseado em: https://stackoverflow.com/questions/45006341/xgboost-how-to-use-mae-as-objective-function/45370500
def huber_approx_obj(preds, dtrain):
    d = preds - dtrain.get_labels() #remove .get_labels() for sklearn
    h = 1  #h is delta in the graphic
    scale = 1 + (d / h) ** 2
    scale_sqrt = np.sqrt(scale)
    grad = d / scale_sqrt
    hess = 1 / scale / scale_sqrt
    return grad, hess

def huber_approx_obj(preds, dtrain):
    d = preds - dtrain.get_labels() #remove .get_labels() for sklearn
    h = 1  #h is delta in the graphic
    scale = 1 + (d / h) ** 2
    scale_sqrt = np.sqrt(scale)
    grad = d / scale_sqrt
    hess = 1 / scale / scale_sqrt
    return grad, hess

def huber_approx_objective(preds, dtrain):
    d = preds - dtrain #remove .get_labels() for sklearn
    h = 1  #h is delta in the graphic
    scale = 1 + (d / h) ** 2
    scale_sqrt = np.sqrt(scale)
    grad = d / scale_sqrt
    hess = 1 / scale / scale_sqrt
    return grad, hess


def fair_obj(preds, dtrain):
    """y = c * abs(x) - c**2 * np.log(abs(x)/c + 1)"""
    x = preds - dtrain.get_labels()
    c = 1
    den = abs(x) + c
    grad = c*x / den
    hess = c*c / den ** 2
    return grad, hess

def fair_objective(preds, dtrain):
    """y = c * abs(x) - c**2 * np.log(abs(x)/c + 1)"""
    x = preds - dtrain
    c = 1
    den = abs(x) + c
    grad = c*x / den
    hess = c*c / den ** 2
    return grad, hess


def log_cosh_obj(preds, dtrain):
    x = preds - dtrain.get_labels()
    grad = np.tanh(x)
    hess = 1 / np.cosh(x)**2
    return grad, hess

def log_cosh_objective(preds, dtrain):
    x = preds - dtrain
    grad = np.tanh(x)
    hess = 1 / np.cosh(x)**2
    return grad, hess

def logregobj(preds, dtrain):
    labels = dtrain.get_label()
    preds = 1.0 / (1.0 + np.exp(-preds))
    grad = preds - labels
    hess = preds * (1.0 - preds)
    return grad, hess

def logregobjective(y_true, y_pred):
    labels = y_true
    y_pred = 1.0 / (1.0 + np.exp(-y_pred))
    grad = y_pred - labels
    hess = y_pred * (1.0 - y_pred)
    return grad, hess


def cupScoreObj(y_true, y_pred):
    grad = 20*(y_true**2) - 35*y_true + 50*y_pred
    hess = 50 * (y_pred**0)
    return grad, hess

def evalerror2(preds, dtrain):
    labels = dtrain
    return 'error', float(sum(labels != (preds > 0.0))) / len(labels)

# def evalerror(preds, dtrain):
#     labels = dtrain.get_label()
#     return 'error', float(sum(labels != (preds > 0.0))) / len(labels)

def evalerror(y_predicted, y_true):
    labels = y_true.get_label()
    y_predicted = np.round(y_predicted)
    matrix = metrics.confusion_matrix(labels, y_predicted)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
    return 'cupScore', weightSum

def evalerror_negative(y_predicted, y_true):
    labels = y_true.get_label()
    y_predicted = np.round(y_predicted)
    matrix = metrics.confusion_matrix(labels, y_predicted)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,25,5,-5]))
    return 'cupScore', weightSum


# In[35]:


def logistic_regression():
    """
    F1 score is: 0.7285714285714285
    AUC Score is: 0.9667565771367231
    """
    X_train, X_test, y_train, y_test = data_processor()
    clf = LogisticRegression(C=1e5)
    clf.fit(X_train, y_train)

    print("Score: ", clf.score(X_test, y_test))
    y_pred = clf.predict(X_test)
    y_pred_proba = clf.predict_proba(X_test)[:, 1]
    print("F1 score is: {}".format(f1_score(y_test, y_pred)))
    print("AUC Score is: {}".format(roc_auc_score(y_test, y_pred_proba)))
    matrix = metrics.confusion_matrix(y_test, y_pred)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
    print(f"Score Cup: {weightSum}")
    print(f"Confusion matrix: \n{matrix}")


def logistic_with_smote():
    X_train, X_test, y_train, y_test = data_processor()

    clf = LogisticRegression(C=1e5)
    clf.fit(X_train, y_train)
    # build model with SMOTE imblearn
    smote_pipeline = make_pipeline_imb(SMOTE(random_state=4), clf)

    smote_model = smote_pipeline.fit(X_train, y_train)
    smote_prediction = smote_model.predict(X_test)
    smote_prediction_proba = smote_model.predict_proba(X_test)[:, 1]

    print(classification_report_imbalanced(y_test, smote_prediction))
    print('SMOTE Pipeline Score {}'.format(smote_pipeline.score(X_test, y_test)))
    print("SMOTE AUC score: ", roc_auc_score(y_test, smote_prediction_proba))
    print("SMOTE F1 Score: ", f1_score(y_test, smote_prediction))
    matrix = metrics.confusion_matrix(y_test, smote_prediction)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
    print(f"Score Cup: {weightSum}")
    print(f"Confusion matrix: \n{matrix}")


def randomForest():
    """
    F1 score is: 0.7857142857142857
    AUC Score is: 0.9450972761670293
    """
    X_train, X_test, y_train, y_test = data_processor()
    # parameters = {'n_estimators': [10, 20, 30, 50], 'max_depth': [2, 3, 4]}

    clf = RandomForestClassifier(max_depth=4, n_estimators=20)
    # clf = GridSearchCV(alg, parameters, n_jobs=4)
    clf.fit(X_train, y_train)
    print("Score: ", clf.score(X_test, y_test))
    y_pred = clf.predict(X_test)
    y_pred_proba = clf.predict_proba(X_test)[:, 1]

    print("F1 score is: {}".format(f1_score(y_test, y_pred)))
    print("AUC Score is: {}".format(roc_auc_score(y_test, y_pred_proba)))
    matrix = metrics.confusion_matrix(y_test, y_pred)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
    print(f"Score Cup: {weightSum}")
    print(f"Confusion matrix: \n{matrix}")

    # print("The Features Importance are: ")  # for feature, value in zip(X_train.columns, clf.feature_importances_):
    #     print(feature, value)
    # print(clf.best_estimator_)
    # print(clf.best_params_)
    # print(clf.best_score_)


def neural_nets():
    X_train, X_test, y_train, y_test = data_processor()
    clf = MLPClassifier(hidden_layer_sizes=(100, 100, 100,))

    clf.fit(X_train, y_train)
    print("Score: ", clf.score(X_test, y_test))
    y_pred = clf.predict(X_test)
    y_pred_proba = clf.predict_proba(X_test)[:, 1]
    print("F1 score is: {}".format(f1_score(y_test, y_pred)))
    print("AUC Score is: {}".format(roc_auc_score(y_test, y_pred_proba)))
    matrix = metrics.confusion_matrix(y_test, y_pred)
    weightSum = np.sum(np.reshape(matrix, 4)*np.array([0,-25,-5,5]))
    print(f"Score Cup: {weightSum}")
    print(f"Confusion matrix: \n{matrix}")

